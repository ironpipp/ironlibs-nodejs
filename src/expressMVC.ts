import {IronLibsCommon} from "ironlibs-base";
import express = require('express');

export module IronLibsExpressMVC
{
    let __ : typeof IronLibsCommon = require("ironlibs-base/build/common").IronLibsCommon;        //using ": typeof IronLibsCommon" we get the same as "import {IronLibsCommon as __} from './common';"

    export enum NotificationType
    {
        ERROR   = 0,
        SUCCESS = 1,
        INFO    = 2
    }


    /**
     * The data that normally travels in the net in ajax calls
     */
    export interface IServerResponseModel
    {
        IsValid : boolean,
        Notifications : {
            MessageText : string,
            PropertyName? : string,
            Type : NotificationType
        }[],
        Data? : any
    }



    export class ControllerBase
    {
        constructor()
        {
            this.router = express.Router();
        }

        private readonly router : express.Router = null;

        public GetRouter() : express.Router { return this.router; }


        /**
         * Registers a controller action invoked for ANY http verb
         *
         * @param url one or more urls (also with wildcards) to be matched in order to call the callback
         * @param clbk The action executed upon an url match
         */
        public RegisterActionAll(url : string | string[], clbk : express.RequestHandler)
        {
            let me = this;
            let urls = __.IsArray(url) ? <string[]>url : [<string>url];

            urls.forEach(function(u)
            {
                me.router.all(u, clbk)
            });
        }


        /**
         * Registers a controller action invoked for GET http verb
         *
         * @param url one or more urls (also with wildcards) to be matched in order to call the callback
         * @param clbk The action executed upon an url match
         */
        public RegisterActionGet(url : string | string[], clbk : express.RequestHandler)
        {
            let me = this;
            let urls = __.IsArray(url) ? <string[]>url : [<string>url];

            urls.forEach(function(u)
            {
                me.router.get(u, clbk)
            });
        }


        /**
         * Registers a controller action invoked for POST http verb
         *
         * @param url one or more urls (also with wildcards) to be matched in order to call the callback
         * @param clbk The action executed upon an url match
         */
        public RegisterActionPost(url : string | string[], clbk : express.RequestHandler)
        {
            let me = this;
            let urls = __.IsArray(url) ? <string[]>url : [<string>url];

            urls.forEach(function(u)
            {
                me.router.post(u, clbk)
            });
        }


        public Respond(res : express.Response, msg : string | IServerResponseModel, statusCode : number = 200, headerValues : object = null) : void
        {
            res.statusCode = statusCode;

            if (__.IsNotNullObject(headerValues))
            {
                for (let key in headerValues)
                {
                    if (!headerValues.hasOwnProperty(key))
                        continue;
                    res.header(key, headerValues[key]);
                }
            }

            res.status(statusCode);

            if (__.IsNull(msg))
                msg = {IsValid : (statusCode >= 200 && statusCode < 300), Notifications : []};

            if (__.IsString(msg))
                res.write(msg);
            else
                res.json(msg);

            res.end();
        }

        public RespondOk(res : express.Response, data : any = null, notification : string = null, type : NotificationType = NotificationType.SUCCESS) : void
        {
            let ret : IServerResponseModel = {
                IsValid       : true,
                Notifications : [],
            };

            if (__.IsNotNull(data))
                ret.Data = data;

            if (__.IsString(notification))
                ret.Notifications.push({MessageText : notification, Type : type});

            this.Respond(res, ret, 200);
        }

        public RespondError(res : express.Response, data : any = null, notification : string = null, clientError : boolean = true) : void
        {
            let ret : IServerResponseModel = {
                IsValid       : false,
                Notifications : [],
            };

            if (__.IsNotNull(data))
                ret.Data = data;

            if (__.IsString(notification))
                ret.Notifications.push({MessageText : notification, Type : NotificationType.ERROR});

            this.Respond(res, ret, clientError ? 400 : 500);
        }

        public RespondRedirect(res : express.Response, where : string) : void
        {
            this.Respond(res, "", 302, {location : where});
        }
    }
}

