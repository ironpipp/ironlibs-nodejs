import fs = require("fs");
import sqlite3 = require("sqlite3");
import {IronLibsCommon} from "ironlibs-base";

export module IronLibsSqlDb
{
    let __ : typeof IronLibsCommon = require("ironlibs-base/build/common").IronLibsCommon;        //using ": typeof IronLibsCommon" we get the same as "import {IronLibsCommon as __} from './common';"

    export enum DB_FIELD_TYPE
    {
        TEXT,
        NUMBER_INTEGER,
        NUMBER_DOUBLE,
        DATE,
        BINARY
    }

    export enum LOAD_DUMP_BEHAVIOUR
    {
        INSERTBULK,
        UPDATEIFFOUND,
        DELETEALLDATA
    }

    export interface IDBFieldDescriptor
    {
        name : string;
        type : DB_FIELD_TYPE;
        primary? : boolean;
    }


    export interface IEntity
    {
        SqlTableName() : string;

        SqlTableFields() : IDBFieldDescriptor[];

        SqlTableFieldNames() : string[];
    }


    export class Entity implements IEntity
    {
        constructor() {}

        public SqlTableName() : string
        {
            return "Entity";
        }

        public SqlTableFields() : IDBFieldDescriptor[]
        {
            return [];
        }

        public SqlTableFieldNames() : string[]
        {
            return this.SqlTableFields().map(function(el) {return el.name;});
        }
    }

    export class IdEntity extends Entity
    {
        public Id : string;

        public SqlTableName() : string
        {
            return "IdEntity";
        }

        public SqlTableFields() : IDBFieldDescriptor[]
        {
            return [
                {name : "Id", type : DB_FIELD_TYPE.TEXT, primary : true},
            ];
        }
    }

    export class EntityCollection<T extends Entity>
    {
        constructor(private tconstructor : Function,
                    private fkName : string,
                    private values : T[]       = [],
                    private isLoaded : boolean = false)
        {
            if (__.IsArray(values))
                this.values = values;
            else
                this.values = [];
        }

        public Values() : T[]
        {
            return this.values;
        }

        public IsLoaded() : boolean
        {
            return this.isLoaded;
        }

        public FkName() : string
        {
            return this.fkName;
        }

        public Tconstructor() : Function
        {
            return this.tconstructor;
        }

        public LoadWithValues(values : T[])
        {
            let me = this;
            me.values = [];
            values.forEach(function(el)
            {
                me.values.push(el);
            });
            me.isLoaded = true;
        }
    }

    /*#################################################################################################################*/

    /*#################################################################################################################*/



    export class DB
    {
        private db : sqlite3.Database;
        private opt : DB.IOptions;
        private instanceId : string;
        private lastSuccessfulDump : Date = null;
        private automaticDumpTimer : NodeJS.Timer = null;
        private automaticDumpPaused : boolean = false;
        private automaticDumpIsProcessing : boolean = false;
        private isClosed : boolean = false;

        /**
         *
         * @param options Sets the following possible options:
         *        fileName : Specifies the filename of the stored database file, or when inMemoryMode == true specifies the filename of the dump file.
         *        verbose : Specifies whether to log SQL statements or not.
         *        loggingFunction : Specifies the logging function accepting the string to log.
         *        inMemoryMode : If FALSE data is stored immediately (BAD queries performances, especially for non-SSD hard disks).
         *                       If TRUE data is kept in memory and persisted only in intervals of inMemoryDumpEveryMs milliseconds.
         *        inMemoryDumpEveryMs : Has sense only when inMemoryMode == true.
         *                              If > 0  data is persisted automatically as a dump every the specified interval in milliseconds.
         *                              If <= 0 data is NEVER persisted automatically.
         *        inMemoryDumpOnlyIfDirty : If TRUE the automatic dump is updated only if data has changed in any repository.
         *                                  If FALSE the dump is always updated.
         *        inMemoryRepositoriesToDump : The repositories to use to dump data. WARNING: only data of these repositories will be persisted automatically!
         */
        constructor(options : DB.IOptions = {})
        {
            let me = this;

            me.opt = __.MergeObj(DB.DefaultOptions, options);

            me.instanceId = __.Guid.New();

            //OPTIONS ERRORS CHECK
            if (me.opt.inMemoryMode && !__.IsArray(me.opt.inMemoryEntitiesToDump))
                throw new Error("When inMemoryMode == true then inMemoryRepositoriesToDump must be a valid array");
            if (me.opt.verbose && !__.IsFunction(me.opt.loggingFunction))
                throw new Error("When verbose == true then loggingFunction must be a valid function");

            me.db = new sqlite3.Database(me.opt.inMemoryMode ? ":memory:" : me.opt.fileName);

            //Start automatic dump?
            if (me.opt.inMemoryMode && me.opt.inMemoryDumpEveryMs > 0)
            {
                me.automaticDumpTimer = setInterval(function()
                {
                    if (me.automaticDumpPaused || me.automaticDumpIsProcessing)
                        return;

                    me.automaticDumpIsProcessing = true;

                    me.PerformDump(me.opt.fileName, me.opt.inMemoryEntitiesToDump, me.opt.inMemoryDumpOnlyIfDirty);

                }, me.opt.inMemoryDumpEveryMs);

                if (me.opt.verbose)
                    me.opt.loggingFunction("[DB " + me.opt.fileName + "] Started automatic dump every " + me.opt.inMemoryDumpEveryMs + "ms");
            }
        }

        public GetInstanceId() : string
        {
            return this.instanceId;
        }

        public GetIsVerbose() : boolean
        {
            return this.opt.verbose;
        }

        public GetInMemoryEntitiesToDump() : Function[]
        {
            return this.opt.inMemoryEntitiesToDump;
        }

        public GetLoggingFunction() : (msg : string) => void
        {
            return this.opt.loggingFunction;
        }

        public GetFileName() : string
        {
            return this.opt.fileName;
        }

        public GetLastSuccessfulDump() : Date
        {
            return this.lastSuccessfulDump;
        }


        public ExecuteSelect(sql : string, onEnd : (data : any[], err : Error) => void) : void
        {
            let me = this;
            let ret = [];

            if (this.opt.verbose)
                this.opt.loggingFunction("[DB " + this.opt.fileName + "] Execute SELECT: " + sql);

            me.db.each(sql, function(err, row)
                {
                    ret.push(row);
                },
                function(err : Error, count : number)
                {
                    if (__.IsFunction(onEnd))
                        onEnd(ret, err);
                });
        }


        public ExecuteSql(sql : string, callback? : (err : Error) => void)
        {
            if (this.opt.verbose)
                this.opt.loggingFunction("[DB " + this.opt.fileName + "] Execute SQL: " + sql);

            this.db.run(sql, callback);
        }


        public Transaction(callback? : () => void, serial : boolean = true) : void
        {
            if (serial)
                this.db.serialize(callback);
            else
                this.db.parallelize(callback);
        }


        /**
         * Pauses an already started automatic dump
         */
        public PauseAutomaticDump()
        {
            this.automaticDumpPaused = true;
        }

        /**
         * Pauses an already paused automatic dump
         */
        public RestoreAutomaticDump()
        {
            this.automaticDumpPaused = false;
        }


        /**
         * Executes a dump independently from the automatic system.
         *
         * @param fileName Specify the file to be saved. If NULL then options.fileName will be used
         * @param specificEntitiesToDump Specify the entities types to dump. If NULL then options.inMemoryEntitiesToDump will be used
         * @param onEnd Called with non-null "err" in case of error
         */
        public ForceDump(fileName? : string, specificEntitiesToDump? : Function[], onEnd? : (err : Error) => void)
        {
            if (!__.IsArray(specificEntitiesToDump))
                specificEntitiesToDump = this.opt.inMemoryEntitiesToDump;

            if (__.IsEmptyString(fileName))
                fileName = this.opt.fileName;

            this.PerformDump(fileName, specificEntitiesToDump, false, onEnd);
        }

        public LoadDump(dump : string, entitiesToLoad? : Function[], behaviour? : LOAD_DUMP_BEHAVIOUR, onEnd? : (err : Error) => void);
        public LoadDump(dump : any, entitiesToLoad? : Function[], behaviour : LOAD_DUMP_BEHAVIOUR = LOAD_DUMP_BEHAVIOUR.INSERTBULK, onEnd : (err : Error) => void = null)
        {
            let me = this;

            if (__.IsNull(dump))
                dump = this.opt.fileName;

            if (!__.IsArray(entitiesToLoad))
                entitiesToLoad = this.opt.inMemoryEntitiesToDump;

            let handleData = function(data : any)
            {
                let restoreNextRepo = function(which : number, previousError : Error)
                {
                    if (which == entitiesToLoad.length)
                    {
                        //collection finished!
                        if (__.IsFunction(onEnd))
                            onEnd(previousError);

                        return;
                    }

                    let entityRepo = Repository.GetInstance(entitiesToLoad[which], me);

                    entityRepo.LoadDump(data, behaviour, (err : Error) =>
                    {
                        //go on on errors...
                        restoreNextRepo((which + 1), previousError || err);
                    });

                };

                restoreNextRepo(0, null);
            };

            //need to read data from a file? If so let's do it ONLY ONCE, don't delegate each repository!
            if (__.IsNotEmptyString(dump))
            {
                fs.readFile(dump, "utf-8", (err : NodeJS.ErrnoException, data : string) =>
                {
                    if (__.IsNotNull(err))
                    {
                        if (__.IsFunction(onEnd))
                            onEnd(err);
                        return;
                    }
                    handleData(JSON.parse(data));
                });
            }
            else
                handleData(dump);

        }


        /**
         * Eventually CONVERTS the data to SQL-Style based on its TYPE
         */
        public static FormatFieldValueToSql(data : any, type : DB_FIELD_TYPE) : string
        {
            if (type == DB_FIELD_TYPE.DATE)
            {
                if (__.IsDate(data))
                    return (<Date>data).getTime().toString();
                else
                    return "NULL";
            }
            else if (type == DB_FIELD_TYPE.NUMBER_INTEGER || type == DB_FIELD_TYPE.NUMBER_DOUBLE)
            {
                if (__.IsNumber(data))
                    return data.toString();
                else
                    return "NULL";
            }
            else if (type == DB_FIELD_TYPE.TEXT)
            {
                if (__.IsString(data))
                    return '"' + data.replace(/"/g, "\"\"") + '"';
                else
                    return "NULL";
            }
            return "NULL";
        }



        /**
         * check if the file is already present, in this case load it and MERGE the content replacing the entities in specified dataToSave
         *
         */
        public static SaveOrMergeDumpDataToFile(dataToSave : any, fileName : string, onEnd? : (err : Error) => void) : void
        {
            let writeFile = function(data)
            {
                //Save the dump file
                fs.writeFile(fileName, JSON.stringify(data), onEnd);
            };
            if (fs.existsSync(fileName))
            {
                fs.readFile(fileName, "utf-8", (err : NodeJS.ErrnoException, foundData : string) =>
                {
                    if (__.IsNotNull(err))
                    {
                        if (__.IsFunction(onEnd))
                            onEnd(err);
                        return;     //STOP on error
                    }
                    let foundDataObj = JSON.parse(foundData);

                    //REPLACE entities to dump keeping found data
                    dataToSave = __.MergeObj(foundDataObj, dataToSave);

                    writeFile(dataToSave);
                });
            }
            else
                writeFile(dataToSave);

        }

        public static DefaultOptions : DB.IOptions = {
            fileName                : "database.sqlite3",
            verbose                 : false,
            loggingFunction         : (msg : string) => { __.Log(msg); },
            inMemoryMode            : true,
            inMemoryDumpEveryMs     : 2000,
            inMemoryDumpOnlyIfDirty : true,
            inMemoryEntitiesToDump  : []
        };

        public Close(callback? : (err : Error | null) => void) : boolean
        {
            let me = this;

            if (me.isClosed)
                return false;

            this.db.close((err) =>
            {
                me.isClosed = true;

                if (__.IsNotNull(me.automaticDumpTimer))
                    clearInterval(me.automaticDumpTimer);

                if (__.IsFunction(callback))
                    callback(err);
            });

            return true;
        }

        /*##########################################################################
         #########################   PROTECTED STUFF   #########################
         ##########################################################################*/

        protected PerformDump(fileToSave : string, entitiesToDump : Function[], dumpOnlyIfDirty : boolean, onEnd? : (err : Error) => void)
        {
            let me = this;
            if (me.opt.verbose)
                me.opt.loggingFunction("[DB " + me.opt.fileName + "] Running automatic dump");

            let dataToSave = {};
            let reposToSetNotDirty : Repository<Entity>[] = [];

            let dumpNextRepo = function(which : number)
            {
                if (which == entitiesToDump.length)
                {
                    //collection of repos finished!

                    //check if there's something to save
                    let toSaveCount = 0;
                    for (let k in dataToSave)
                    {
                        if (!dataToSave.hasOwnProperty(k))
                            continue;
                        toSaveCount++;
                    }
                    if (toSaveCount == 0)
                    {
                        if (me.opt.verbose)
                            me.opt.loggingFunction("[DB " + me.opt.fileName + "] Nothing to save because nothing is dirty. Automatic dump completed!");

                        me.lastSuccessfulDump = new Date();
                        me.automaticDumpIsProcessing = false;

                        if (__.IsFunction(onEnd))
                            onEnd(null);
                        return;
                    }

                    //SAVE DATA!
                    DB.SaveOrMergeDumpDataToFile(dataToSave, fileToSave, function(writeErr)
                    {
                        if (__.IsNull(writeErr))
                        {
                            if (me.opt.verbose)
                                me.opt.loggingFunction("[DB " + me.opt.fileName + "] Automatic dump completed!");

                            me.lastSuccessfulDump = new Date();
                            me.automaticDumpIsProcessing = false;
                            reposToSetNotDirty.forEach((repoToSetNotDirty : Repository<Entity>) =>
                            {
                                repoToSetNotDirty.SetIsDirty(false);
                            });
                        }
                        else if (me.opt.verbose)
                            me.opt.loggingFunction("[DB " + me.opt.fileName + "] ERROR loading found dump before save: " + JSON.stringify(writeErr));

                        if (__.IsFunction(onEnd))
                        {
                            onEnd(writeErr);
                        }
                    });

                    return;
                }

                let entityRepo = Repository.GetInstance(entitiesToDump[which], me);
                let entityName = Repository.GetEntityName(entitiesToDump[which]);

                //check if must skip this repo because it's not dirty
                if (dumpOnlyIfDirty == true && entityRepo.GetIsDirty() == false)
                {
                    if (me.opt.verbose)
                        me.opt.loggingFunction("[DB " + me.opt.fileName + "] Skipping dump for not dirty repository \"" + entityName + "\".");

                    dumpNextRepo(which + 1);
                    return;
                }

                reposToSetNotDirty.push(entityRepo);

                entityRepo.GetAll((data, err : Error) =>
                {
                    if (__.IsNotNull(err))
                    {
                        if (me.opt.verbose)
                            me.opt.loggingFunction("[DB " + me.opt.fileName + "] ERROR during automatic dump!");

                        me.automaticDumpIsProcessing = false;

                        if (__.IsFunction(onEnd))
                            onEnd(err);

                        return;                     //STOP on first error
                    }

                    dataToSave[entityName] = data;  //keep data to be saved

                    dumpNextRepo(which + 1);
                });

            };

            dumpNextRepo(0);
        }
    }



    export module DB
    {
        export interface IOptions
        {
            fileName? : string,
            verbose? : boolean,
            loggingFunction? : (msg : string) => void,

            inMemoryMode? : boolean,

            //AUTO DUMP settings
            inMemoryDumpEveryMs? : number,
            inMemoryDumpOnlyIfDirty? : boolean,
            inMemoryEntitiesToDump? : Function[]
        }
    }


    /*#################################################################################################################*/

    /*#################################################################################################################*/



    export class Repository<T extends Entity>
    {
        private SqlTableName : string;
        private SqlTableFields : IDBFieldDescriptor[];
        private instanceId : string;
        private isDirty : boolean = false;

        private static reposInstancesCache : { [id : string] : Repository<Entity> } = {};

        public static GetInstance<T extends Entity>(Tconstructor : Function, db : DB) : Repository<T>
        {
            let cacheKey = Repository.GetCacheKey(Tconstructor, db);
            if (__.IsNull(Repository.reposInstancesCache[cacheKey]))
                Repository.reposInstancesCache[cacheKey] = new Repository<T>(Tconstructor, db);

            return <Repository<T>>(Repository.reposInstancesCache[cacheKey]);
        }

        protected static GetCacheKey(Tconstructor : Function, db : DB) : string
        {
            return Repository.GetEntityName(Tconstructor) + "_" + db.GetInstanceId();
        }


        /**
         * DON'T USE directly THIS!
         *
         * @param Tconstructor
         * @param db
         */
        constructor(private Tconstructor, private db : DB)
        {
            let me = this;

            let cacheKey = Repository.GetCacheKey(Tconstructor, db);
            if (__.IsNotNull(Repository.reposInstancesCache[cacheKey]))
                throw new Error("Error: Repository instantiation failed: use Repository.GetInstance() instead of new.");
            else
                Repository.reposInstancesCache[cacheKey] = this;

            me.instanceId = __.Guid.New();

            let t = new Tconstructor();
            me.SqlTableName = t.SqlTableName();
            me.SqlTableFields = t.SqlTableFields();
        }



        public static GetEntityName(typeConstructor : Function) : string
        {
            let txt = typeConstructor.toString();
            let startPos = txt.indexOf(" ") + 1;
            return txt.substr(startPos, txt.indexOf("(") - startPos);
        }


        public GetIsDirty() : boolean
        {
            return this.isDirty;
        }


        public SetIsDirty(dirtyness : boolean) : void
        {
            this.isDirty = dirtyness;
        }


        /**
         * Ensures that the data table for the repository type is present
         * @param forceRecreate If TRUE all found table data will be erased
         * @param onEnd
         */
        public Init(forceRecreate : boolean = false, onEnd? : (err : Error) => void)
        {
            let me = this;

            if (forceRecreate == true)
                me.RecreateTable(onEnd);
            else
                me.db.ExecuteSql(me.GetSqlCreateTable(true), onEnd);
        }


        /**
         * Destroys if exists and recreates the empty table.
         * Useful to DELETE ALL ITS DATA.
         */
        public RecreateTable(onEnd? : (err : Error) => void)
        {
            let me = this;
            this.db.Transaction(function()
            {
                me.db.ExecuteSql("DROP TABLE IF EXISTS " + me.SqlTableName, (err : Error) =>
                {
                    if (__.IsNotNull(err))
                    {
                        if (__.IsFunction(onEnd))
                            onEnd(err);
                    }
                    else
                    {
                        me.isDirty = true;
                        me.db.ExecuteSql(me.GetSqlCreateTable(false), onEnd);
                    }
                });
            });
        }



        public LoadDump(dump : string, behaviour? : LOAD_DUMP_BEHAVIOUR, onEnd? : (err : Error) => void);
        public LoadDump(dump : any, behaviour : LOAD_DUMP_BEHAVIOUR = LOAD_DUMP_BEHAVIOUR.INSERTBULK, onEnd : (err : Error) => void = null)
        {
            let me = this;
            let myTypeName = Repository.GetEntityName(me.Tconstructor);

            if (__.IsNull(dump))
                dump = me.db.GetFileName();

            if (me.db.GetIsVerbose())
                me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] Loading dump for type \"" + myTypeName + "\"");


            let handleData = function(data : any)
            {
                //ok, here we have the dump data, try to cast to my type
                let castedData : T[] = data[myTypeName].map((record) =>
                {
                    return me.ConvertObjectToType(record);
                });


                let insertData = function()
                {
                    if (behaviour == LOAD_DUMP_BEHAVIOUR.UPDATEIFFOUND)
                        me.SaveAll(castedData, (result : boolean[], err : Error) =>
                        {
                            if (me.db.GetIsVerbose())
                            {
                                if (__.IsNull(err))
                                    me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] Dump loaded checking to update!");
                                else
                                    me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] ERROR loading dump: " + JSON.stringify(err));
                            }

                            if (__.IsFunction(onEnd))
                                onEnd(err);
                        });
                    else
                        me.InsertBulk(castedData, (err : Error) =>
                        {
                            if (me.db.GetIsVerbose())
                            {
                                if (__.IsNull(err))
                                    me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] Dump loaded with InsertBulk!");
                                else
                                    me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] ERROR loading dump: " + JSON.stringify(err));
                            }

                            if (__.IsFunction(onEnd))
                                onEnd(err);
                        });
                };

                //now check if need to delete previous data
                if (behaviour == LOAD_DUMP_BEHAVIOUR.DELETEALLDATA)
                {
                    me.RecreateTable((err : Error) =>
                    {
                        if (__.IsNotNull(err))
                        {
                            if (me.db.GetIsVerbose())
                                me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] ERROR loading dump: " + JSON.stringify(err));

                            if (__.IsFunction(onEnd))
                                onEnd(err);

                            return;     //STOP on error
                        }
                        insertData();
                    });
                }
                else
                    insertData();


            };

            //need to read data from a file?
            if (__.IsNotEmptyString(dump))
            {
                fs.readFile(dump, "utf-8", (err : NodeJS.ErrnoException, data : string) =>
                {
                    if (__.IsNotNull(err))
                    {
                        if (me.db.GetIsVerbose())
                            me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] ERROR loading dump: " + JSON.stringify(err));

                        if (__.IsFunction(onEnd))
                            onEnd(err);

                        return;     //STOP on error
                    }
                    handleData(JSON.parse(data));
                });
            }
            else
                handleData(dump);
        }



        /**
         * Checks if an Entity exists in this repository (search by PrimaryKey).
         *
         * @param pk  an Entity PrimaryKey
         * @param onEnd Called with "result" containing the check, or with non-null "err" in case of error
         */
        public Exists(pk : any, onEnd : (result : boolean, err : Error) => void) : void
        {
            let me = this;
            if (!__.IsFunction(onEnd))
                onEnd = () => {};

            let pkInfo = me.SqlTableFields.filter((x) => x.primary)[0];
            if (__.IsNull(pkInfo))
            {
                setTimeout(function()
                {
                    onEnd(null, new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }

            let sql = "select count(*) from " + me.SqlTableName + " where [" + pkInfo.name + "] = " + DB.FormatFieldValueToSql(pk, pkInfo.type) + ";";

            me.db.ExecuteSelect(sql, function(data : any[], err : Error)
            {
                let result = false;
                if (__.IsNull(err))
                {
                    result = data[0]["count(*)"] == 1;
                }

                if (__.IsFunction(onEnd))
                    onEnd(result, err);

            });
        }



        /**
         * Deletes the passed entity (search by PrimaryKey) IF IT EXISTS in this repository.
         *
         * @param pk  an Entity PrimaryKey
         * @param onEnd Called with non-null "err" in case of error
         */
        public Delete(pk : any, onEnd : (err : Error) => void) : void
        {
            let me = this;
            if (!__.IsFunction(onEnd))
                onEnd = () => {};

            let pkInfo = me.SqlTableFields.filter((x) => x.primary)[0];
            if (__.IsNull(pkInfo))
            {
                setTimeout(function()
                {
                    onEnd(new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }

            let sql = "DELETE from " + me.SqlTableName + " where [" + pkInfo.name + "] = " + DB.FormatFieldValueToSql(pk, pkInfo.type) + ";";
            me.db.ExecuteSql(sql, (err) =>
            {
                if (__.IsNull(err))
                    me.SetIsDirty(true);

                onEnd(err);
            });
        }



        public GetCount(onEnd : (itemsCount : number, err : Error) => void, whereClause? : string)
        {
            let me = this;
            let sql = "SELECT count(*) from " + me.SqlTableName;
            if (__.IsNotEmptyString(whereClause))
                sql += " WHERE " + whereClause;

            me.db.ExecuteSelect(sql, function(countData : any[], err : Error)
            {
                let count = -1;
                if (__.IsNull(err))
                    count = countData[0]["count(*)"];
                if (__.IsFunction(onEnd))
                    onEnd(count, err);
            });
        }


        /**
         * Returns ALL entities in this repository.
         *
         * @param onEnd Called with "data" containing the entities, or with non-null "err" in case of error
         * @param collectionsToFill Specify which collections (properties of the retrieved entities) to retrieve
         */
        public GetAll(onEnd : (data : T[], err : Error) => void, collectionsToFill : string[] = null) : void
        {
            this.GetWhere("", onEnd, collectionsToFill);
        }



        /**
         * Returns ALL entities in this repository satisfying the passed clause
         *
         * @param whereClause The SQL where clause
         * @param onEnd Called with "data" containing the entities, or with non-null "err" in case of error
         * @param collectionsToFill Specify which collections (properties of the retrieved entities) to retrieve
         * @param onlySpecifiedFields
         */
        public GetWhere(whereClause : string,
                        onEnd : (data : T[], err : Error) => void,
                        collectionsToFill : string[]   = null,
                        onlySpecifiedFields : string[] = null) : void
        {
            let me = this;
            if (__.IsNull(collectionsToFill))
                collectionsToFill = [];

            if (!__.IsFunction(onEnd))
                onEnd = () => {};

            let pkInfo = me.SqlTableFields.filter((x) => x.primary)[0];
            if (__.IsNull(pkInfo))
            {
                setTimeout(function()
                {
                    onEnd(null, new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }

            let sql = "SELECT ";
            if (__.IsNotEmptyArray(onlySpecifiedFields))
                sql += onlySpecifiedFields.map((x) => "[" + x + "]").join(",");
            else
                sql += "*";

            sql += " from " + me.SqlTableName;

            if (__.IsNotEmptyString(whereClause))
                sql += " WHERE " + whereClause;

            me.db.ExecuteSelect(sql, function(data : any[], err : Error)
            {
                let mainResult : T[] = null;
                if (__.IsNull(err))
                {
                    mainResult = data.map(function(dbRow)
                    {
                        return me.ConvertObjectToType(dbRow);
                    });
                }

                let asyncFunctions = [];


                //now check for COLLECTIONS to be retrieved
                if (__.IsArray(mainResult))
                {
                    mainResult.forEach(function(record : T)
                    {
                        collectionsToFill.forEach(function(collectionFieldName : string)
                        {
                            let collection = <EntityCollection<Entity>>(record[collectionFieldName]);
                            let repo = Repository.GetInstance(collection.Tconstructor(), me.db);

                            asyncFunctions.push(function(notifyEnd : Function)
                            {
                                //Get the elements of the collection
                                //TODO: here it's possible to select some nested collections...
                                repo.GetWhere(collection.FkName() + " = " + DB.FormatFieldValueToSql(record[pkInfo.name], pkInfo.type),
                                    function(data : T[], err : Error)
                                    {
                                        if (__.IsNull(err))
                                        {
                                            //replace the collection with the retrieved values
                                            collection.LoadWithValues(data);
                                        }
                                        notifyEnd(err);     //return the error if present
                                    });
                            });

                        });
                    });
                }

                //the JOINING function
                asyncFunctions.push(function(resultsOrderedByPosition : IronLibsCommon.ExecuteAndJoin.Result[], resultsOrderedByTime : IronLibsCommon.ExecuteAndJoin.Result[])
                {
                    let errToReturn = Repository.GetFirstNotNullError(err, resultsOrderedByTime);

                    onEnd(mainResult, errToReturn);     //pass the FIRST error received
                });

                __.ExecuteAndJoin(asyncFunctions);
            });
        }


        /**
         * Returns the entity with the specified PrimaryKey in this repository.
         *
         * @param pk
         * @param onEnd Called with "data" containing the entity if found or NULL if not found,
         * or with non-null "err" in case of error
         * @param collectionsToFill
         */
        public GetByPk(pk : any, onEnd : (data : T, err : Error) => void, collectionsToFill : string[] = null) : void
        {
            let me = this;
            if (!__.IsFunction(onEnd))
                onEnd = () => {};

            let pkInfo = me.SqlTableFields.filter((x) => x.primary)[0];
            if (__.IsNull(pkInfo))
            {
                setTimeout(function()
                {
                    onEnd(null, new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }

            this.GetWhere(
                "[" + pkInfo.name + "] = " + DB.FormatFieldValueToSql(pk, pkInfo.type),
                function(data : any[], err : Error)
                {
                    let result : T = null;

                    if (__.IsNull(err) && data.length > 0)
                    {
                        result = data[0];
                    }

                    if (__.IsFunction(onEnd))
                        onEnd(result, err);
                },
                collectionsToFill);
        }



        /**
         * Returns the entities with the specified PrimaryKeys in this repository.
         *
         * @param pks
         * @param onEnd Called with "data" containing the entities found, or with non-null "err" in case of error
         * @param collectionsToFill Specifies which collections (properties of the retrieved entities) to retrieve
         */
        public GetByPks(pks : any[], onEnd : (data : T[], err : Error) => void, collectionsToFill : string[] = null) : void
        {
            let me = this;
            if (!__.IsFunction(onEnd))
                onEnd = () => {};

            let pkInfo = me.SqlTableFields.filter((x) => x.primary)[0];
            if (__.IsNull(pkInfo))
            {
                setTimeout(function()
                {
                    onEnd(null, new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }

            let idsList = pks.map((pk) => {return DB.FormatFieldValueToSql(pk, pkInfo.type); });

            this.GetWhere(
                "[" + pkInfo.name + "] in (" + idsList.join(",") + ")",
                onEnd,
                collectionsToFill);
        }



        /**
         * Executes an OPTIIMIZED bulk INSERT for all passed entities which MUST BE NEW.
         * WARNING: if any of them already exist then an error will be returned and NO entity will be inserted!
         *
         * @param items The NEW entities
         * @param onEnd Called with non-null "err" in case of error
         */
        public InsertBulk(items : T[], onEnd? : (err : Error) => void) : void
        {
            let me = this;
            let sql = "INSERT INTO " + me.SqlTableName + "(";

            me.SqlTableFields.forEach(function(field, i)
            {
                sql += "[" + field.name + "]";
                if (i < me.SqlTableFields.length - 1)
                    sql += ",";
            });
            sql += ") VALUES (";

            items.forEach(function(data, dataIdx)
            {
                sql += me.GetSqlEntityValues(data) + ")";

                if (dataIdx < items.length - 1)
                    sql += ",(";
            });

            me.db.ExecuteSql(sql, (err) =>
            {
                if (__.IsNull(err))
                    me.SetIsDirty(true);

                if (__.IsFunction(onEnd))
                    onEnd(err);
            });
        }



        /**
         * Persists the passed entities.
         * Expects a Not-Null items, otherwise the callback will be called with a specific error.
         * A CHECK is made whether each item needs to be INSERTED or UPDATED.
         * WARNING: not so performing... since (2*items.length) queries are performed.
         *
         * @param items The entities to be persisted
         * @param onEnd  Called with "created" == true if INSERT, == false if UPDATE for each item, or with non-null "err" in case of error
         * @param collectionsToSave Specifies which collections (properties of the saved entities) to save
         */
        public SaveAll(items : T[], onEnd? : (created : boolean[], err : Error) => void, collectionsToSave : string[] = null) : void
        {
            let me = this;
            let asyncFunctions = [];
            let resultsCreated = [];

            items.forEach(function(item : T, itemIdx : number)
            {
                asyncFunctions.push(function(notifyEnd : Function)
                {
                    //Save ALL the elements in ANY order
                    me.Save(item, function(created : boolean, err : Error)
                    {
                        resultsCreated[itemIdx] = created;      //store the "created" information at the right position
                        notifyEnd(err);                         //return the error if present
                    }, collectionsToSave);
                });
            });

            //the JOINING function
            asyncFunctions.push(function(resultsOrderedByPosition : IronLibsCommon.ExecuteAndJoin.Result[], resultsOrderedByTime : IronLibsCommon.ExecuteAndJoin.Result[])
            {
                if (__.IsFunction(onEnd))
                {
                    let errToReturn = Repository.GetFirstNotNullError(null, resultsOrderedByTime);
                    onEnd(resultsCreated, errToReturn);         //pass the FIRST error received and the "created" array
                }
            });

            __.ExecuteAndJoin(asyncFunctions);
        }



        /**
         * Persists the passed entity.
         * Expects a Not-Null obj, otherwise the callback will be called with a specific error.
         * A CHECK is made whether it needs to be INSERTED or UPDATED.
         * WARNING: not so performing... since 2 queries are performed.
         *
         * @param obj The entity to be persisted
         * @param onEnd  Called with "created" == true if INSERT, == false if UPDATE, or with non-null "err" in case of error
         * @param collectionsToSave Specifies which collections (properties of the saved entity) to save
         */
        public Save(obj : T, onEnd? : (created : boolean, err : Error) => void, collectionsToSave : string[] = null) : void
        {
            let me = this;
            if (__.IsNull(collectionsToSave))
                collectionsToSave = [];
            if (!__.IsFunction(onEnd))
                onEnd = () => {};

            if (__.IsNull(obj))
            {
                setTimeout(function()
                {
                    onEnd(false, new Error("NULL obj passed to Repository.Save"));
                }, 1);
                return;
            }

            let pkInfo = me.SqlTableFields.filter((x) => x.primary)[0];
            if (__.IsNull(pkInfo))
            {
                setTimeout(function()
                {
                    onEnd(false, new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }

            //must CHECK if needs to be INSERTED or UPDATED
            me.Exists(obj[pkInfo.name], function(exists : boolean, err : Error)
            {
                if (__.IsNotNull(err))
                {
                    onEnd(false, err);
                    return;
                }


                /**
                 * Internal function which fires async save of all collectionsToSave
                 * @param finalCreated Specifies what to pass to onEnd
                 */
                let SaveAllCollections = function(finalCreated : boolean)
                {
                    let asyncFunctions = [];

                    collectionsToSave.forEach(function(collectionFieldName : string)
                    {
                        let collection = <EntityCollection<Entity>>(obj[collectionFieldName]);
                        let repo = Repository.GetInstance(collection.Tconstructor(), me.db);
                        let subitemsToSave = obj[collectionFieldName].Values();
                        subitemsToSave.forEach((el) => {el[collection.FkName()] = obj[pkInfo.name]; });       //link all items in the collection to their parent

                        asyncFunctions.push(function(notifyEnd : Function)
                        {
                            repo.SaveAll(subitemsToSave, function(created : boolean[], err2 : Error)
                            {
                                notifyEnd(err2);
                            });
                        });
                    });

                    //the JOINING function
                    asyncFunctions.push(function(resultsOrderedByPosition : IronLibsCommon.ExecuteAndJoin.Result[], resultsOrderedByTime : IronLibsCommon.ExecuteAndJoin.Result[])
                    {
                        let errToReturn = Repository.GetFirstNotNullError(null, resultsOrderedByTime);
                        onEnd(finalCreated, errToReturn);         //pass the FIRST error received and the "created" array
                    });

                    __.ExecuteAndJoin(asyncFunctions);
                };


                if (exists)
                {
                    //create the sql statement
                    let sql = "UPDATE " + me.SqlTableName + " set ";
                    me.SqlTableFields.forEach(function(field, i)
                    {
                        sql += "[" + field.name + "] = " + DB.FormatFieldValueToSql(obj[field.name], field.type);

                        if (i < me.SqlTableFields.length - 1)
                            sql += ",";
                    });
                    sql += " WHERE [" + pkInfo.name + "] = " + DB.FormatFieldValueToSql(obj[pkInfo.name], pkInfo.type) + ";";

                    me.db.ExecuteSql(sql, function(err : Error)
                    {
                        if (__.IsNotNull(err))
                        {
                            onEnd(false, err);                  // "created" == false, eventual error passed straight away
                            return;
                        }

                        me.SetIsDirty(true);

                        if (collectionsToSave.length == 0)
                        {
                            onEnd(false, err);                  // "created" == false, eventual error passed straight away
                        }
                        else
                            SaveAllCollections(false);      //save was OK, go on with specified collections
                    });
                }
                else
                {
                    me.InsertBulk([obj], function(err : Error)
                    {
                        if (__.IsNotNull(err) || collectionsToSave.length == 0)
                        {
                            if (__.IsFunction(onEnd))
                                onEnd(true, err);                  // "created" == false, eventual error passed straight away
                        }
                        else
                            SaveAllCollections(true);              //save was OK, go on with specified collections
                    });
                }
            });
        }



        /*##########################################################################
         #########################   PROTECTED STUFF   #########################
         ##########################################################################*/


        /**
         * Returns the first NON null error from the one passed and eventual errors returned by the async functions whose results have been passed
         */
        protected static GetFirstNotNullError(err : any, results : IronLibsCommon.ExecuteAndJoin.Result[]) : any
        {
            if (__.IsNotNull(err))
                return err;
            if (__.IsArray(results))
            {
                for (let i = 0; i < results.length; i++)
                {
                    if (__.IsNotNull(results[i].SuccessfulResult))       //this query returned an error
                        return results[i].SuccessfulResult;
                    if (__.IsNotNull(results[i].Exception))              //this query threw an exception
                        return results[i].Exception;
                }
            }
            return null;
        }



        /**
         * Tries to convert a PLAIN JavaScript object to a T-Typed instance
         */
        protected ConvertObjectToType(obj : any) : T
        {
            let me = this;

            //CONVERT to the specified type
            let objT = new me.Tconstructor();
            me.SqlTableFields.forEach(function(field)
            {
                //Eventually CONVERT the data to be written
                if (field.type == DB_FIELD_TYPE.DATE && __.IsNotNull(obj[field.name]))
                    objT[field.name] = new Date(obj[field.name]);
                else
                    objT[field.name] = obj[field.name];
            });

            return <T>objT;
        }


        /**
         * Returns the fields SQL-VALUES of the given record
         */
        protected GetSqlEntityValues(data : T) : string
        {
            let me = this;
            let sql = "";

            me.SqlTableFields.forEach(function(field, i)
            {
                sql += DB.FormatFieldValueToSql(data[field.name], field.type);

                if (i < me.SqlTableFields.length - 1)
                    sql += ",";
            });

            return sql;
        }



        protected GetSqlCreateTable(ifNotExist : boolean)
        {
            let ret = "CREATE TABLE" +
                (ifNotExist ? " IF NOT EXISTS " : " ") +
                this.SqlTableName +
                "(";

            let fields = this.SqlTableFields;
            let primaryKeyName : string = null;
            fields.forEach(function(f, i)
            {
                ret += "'" + f.name + "' ";
                if (f.type == DB_FIELD_TYPE.TEXT)
                    ret += "TEXT";
                else if (f.type == DB_FIELD_TYPE.NUMBER_INTEGER)
                    ret += "INTEGER";
                else if (f.type == DB_FIELD_TYPE.NUMBER_DOUBLE)
                    ret += "DOUBLE";
                else if (f.type == DB_FIELD_TYPE.DATE)
                    ret += "INTEGER";
                else if (f.type == DB_FIELD_TYPE.BINARY)
                    ret += "BINARY";

                if (f.primary == true)
                {
                    ret += " PRIMARY KEY UNIQUE NOT NULL";
                    primaryKeyName = f.name;
                }

                if (i < fields.length - 1)
                    ret += ", ";
            });
            ret += ");";

            if (__.IsNotEmptyString(primaryKeyName))
                ret += " CREATE INDEX " + this.SqlTableName + "_PK on " + this.SqlTableName + "(" + primaryKeyName + ");";

            return ret;
        }
    }
}
