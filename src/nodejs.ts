import https = require("https");
import http = require("http");
import fs = require("fs");
import path = require('path');
import {IronLibsCommon} from "ironlibs-base";

export module IronLibsNodeJs
{
    let __ : typeof IronLibsCommon = require("ironlibs-base/build/common").IronLibsCommon;        //using ": typeof IronLibsCommon" we get the same as "import {IronLibsCommon as __} from './common';"

    export function IsUnixEnvironment() : boolean
    {
        return process.platform != "win32";
    }

    export function GetDirectoriesSeparator() : string
    {
        return IsUnixEnvironment() ? "/" : "\\";
    }

    /**
     * Returns the APPLICATION DATA directory with the ending separator (ex: "/")
     */
    export function GetDataDirectory(appName : string, ensureExists : boolean = true) : string
    {
        let ret : string;
        let sep = GetDirectoriesSeparator();

        if (__.IsNotEmptyString(process.env.APPDATA))
            ret = __.EnsureEndsWith(process.env.APPDATA, sep) + (__.IsNotEmptyString(appName) ? appName + sep : "");
        else
            ret = __.EnsureEndsWith(process.cwd(), sep) + (__.IsNotEmptyString(appName) ? appName + sep : "");

        if (ensureExists)
        {
            if (!fs.existsSync(ret))
                fs.mkdirSync(ret);
        }

        return ret;
    }


    /**
     * Reads package.json and ensures all required modules are installed. Will exit if one or more is not found.
     */
    export function CheckModules() : void
    {

        let pack;

        try
        {
            pack = require("../package");
        }
        catch (e)
        {
            console.log("ERROR: Could not load package.json from project root. This file is required for reading project properties.");
            process.exit(1);
        }

        let moduleExists = function(modName)
        {
            try
            { return require.resolve(modName); }
            catch (e)
            { return false; }
        };

        let isModuleMissing = false;
        for (let key in pack["dependencies"])
        {
            if (!moduleExists(key))
            {
                isModuleMissing = true;
                console.log("ERROR: Missing module '" + key + "'");
            }
        }

        if (isModuleMissing)
        {
            console.log("ERROR: Required modules are not installed. Run 'npm install' from command line.");
            process.exit(1);
        }

        delete require.cache[pack];
    }


    /**
     * Converts an ArrayBuffer to an UTF8 string
     */
    export function ArrayBufferToUtf8String(buff : ArrayBuffer) : string
    {
        let uInt8Array = new Uint8Array(buff);
        let i = uInt8Array.length;
        let binaryString = new Array(i);
        while (i--)
        {
            binaryString[i] = String.fromCharCode(uInt8Array[i]);
        }

        return binaryString.join('');   //WARNING: returns UTF-8 string!!!
    }


    /**
     * Return the BASE64 string of the SHA1 hash of the passed string
     */
    export function GetHash(toBeHashed : string) : string
    {
        let crypto = require('crypto');

        toBeHashed = __.EnsureString(toBeHashed, "");

        let shasum = crypto.createHash('sha1');
        shasum.update(toBeHashed);

        return shasum.digest('base64');
    }


    let sanitizeFilePath = null;

    /**
     * Accepts only a FILE name (with extension, NO path segments).
     * Returns a valid one.
     * Requires "sanitize-filename" package
     */
    export function SanitizeFileName(fileName : string, replaceInvalidCharsWith = "") : string
    {
        if (__.IsNull(sanitizeFilePath))
            sanitizeFilePath = require("sanitize-filename");

        if (!__.IsString(replaceInvalidCharsWith))
            replaceInvalidCharsWith = "";

        return sanitizeFilePath(fileName, {replacement : replaceInvalidCharsWith});
    }


    /**
     * The SANITIZED paths tree will be returned (without file!). Il will always end with pathSegmentsSeparator.
     * Requires "sanitize-filename" package
     */
    export function SanitizeFolderPath(fullPath : string, replaceInvalidCharsWith = "", pathSegmentsSeparator = GetDirectoriesSeparator()) : string
    {
        if (__.StringEndsWith(fullPath, pathSegmentsSeparator))
            fullPath = fullPath.substr(0, fullPath.length - pathSegmentsSeparator.length);

        let segments = fullPath.split(pathSegmentsSeparator);
        let ret = segments[0] + pathSegmentsSeparator;

        for (let i = 1; i < segments.length; i++)
        {
            segments[i] = SanitizeFileName(segments[i], replaceInvalidCharsWith);
            ret = ret + segments[i] + pathSegmentsSeparator;
        }

        return ret;
    }


    /**
     * Ensures that all subdirectories exist. If one or more are missing creates them.
     *
     * fullPath MUST NOT point to a file, ONLY DIRECTORIES!
     * fullPath CAN contain invalid characters. But only its SANITIZED version is created and returned.
     */
    export function EnsureSanitizedPathsTreeIsCreated(fullPath : string, replaceInvalidCharsWith = "") : string
    {
        let separator = GetDirectoriesSeparator();
        let segments = fullPath.split(separator);
        let ret = segments[0] + separator;

        for (let i = 1; i < segments.length; i++)
        {
            segments[i] = SanitizeFileName(segments[i], replaceInvalidCharsWith);
            ret = ret + segments[i] + separator;

            if (!fs.existsSync(ret))
                fs.mkdirSync(ret);
        }
        return ret;
    }


    /**
     * Checks if the passed string is an absolute path (valid for both UNIX and Windows)
     * Examples of absolute paths: \    \asd\qwe    /asd/qwe    d:\  c:\asd\qwe
     * @param s
     */
    export function IsAbsolutePath(s : string) : boolean
    {
        return (s.length > 0 && (s[0] == "/" || s[0] == "\\")) ||   //ex:  \    \asd\qwe    /asd/qwe
            (s.length > 2 && s[1] == ":" && s[2] == "\\");          //ex:  d:\  c:\qwe
    }


    export function CopyFile(sourceFile : string,
                             destFile : string,
                             onEnd : (err? : Error) => void            = null,
                             deleteSourceAfterSuccessfulCopy : boolean = false) : void
    {
        if (!__.IsFunction(onEnd))
            onEnd = function() {};

        let is = fs.createReadStream(sourceFile);
        let os = fs.createWriteStream(destFile);

        is.on('error', onEnd);
        os.on('error', onEnd);

        is.pipe(os);

        is.on('end', function()
        {
            is.close();
            is.destroy();
            os.close();
            os.destroy();
        });

        is.on('close', function()
        {
            if (deleteSourceAfterSuccessfulCopy)
                fs.unlink(sourceFile, onEnd);
            else
                onEnd(null);
        });
    }

    /**
     * Converts an ArrayBuffer to an UTF8 string
     */
    export function ArrayBufferToBlob(buff : ArrayBuffer) : string
    {
        let uInt8Array = new Uint8Array(buff);
        let i = uInt8Array.length;
        let binaryString = new Array(i);
        while (i--)
        {
            binaryString[i] = String.fromCharCode(uInt8Array[i]);
        }

        return binaryString.join('');   //WARNING: returns UTF-8 string!!!
    }

    export function OpenWithExplorer(path : string) : void
    {
        let cp = require('child_process');
        let exec = cp.exec;

        exec('start "" "' + SanitizeFolderPath(path) + '"');
    }


    /**
     *  Compresses passed data to a valid .7z archive (contains only ONE file named "compressed")
     *  Requires "lzma" package
     *
     * @param dataToCompress The data to compress in any form
     * @param compressionLevel from 1 (fastest) to 9 (best)
     * @param onEnd The "compressedData" param can be converted to a typed array calling "new Uint8Array(compressedData)". The "error" param is 0 upon success or the Error object otherwise
     * @param onProgress The "progress" param goes from 0 to 1
     */
    export function LzmaCompress(dataToCompress : string | number[] | Buffer | Uint8Array,
                                 compressionLevel? : number,
                                 onEnd? : (compressedData : number[], error : Error | number) => void,
                                 onProgress? : (progress : number) => void) : void
    {
        let lzma = require("lzma");

        lzma.compress(dataToCompress, compressionLevel, onEnd, onProgress);
    }



    /**
     *  Decompresses back the passed compressed data
     *  Requires "lzma" package
     *
     * @param compressedData The binary data to decompress
     * @param onEnd The "decompressedData" param is a string if it decodes as valid UTF-8 text, otherwise will be a Uint8Array instance. The "error" param is 0 upon success or the Error object otherwise
     * @param onProgress The "progress" param goes from 0 to 1
     */
    export function LzmaDecompress(compressedData : number[] | Buffer | Uint8Array,
                                   onEnd? : (decompressedData : string | Uint8Array, error : { message : string } | number) => void,
                                   onProgress? : (progress : number) => void) : void
    {
        let lzma = require("lzma");

        lzma.decompress(compressedData, onEnd, onProgress);
    }



    export interface ISimpleWebServerOptions
    {
        port? : number;

        /**
         * The BASE URL PATH the web server will respond to (defaults to "/")
         */
        basePath? : string;

        /**
         * An array of mapping to choose the filesystem directory to use when requesting a web resource.
         * Will be used the FIRST rule matching "pathStartingWith", so put the most generic at the end of the array.
         * "serveFromDirectory" must be an ABSOLUTE path.
         * Example:
         *
         * [{
         *     pathStartingWith   : "/lib/",
         *     serveFromDirectory : path.resolve(__dirname, "..", "node_modules")
         * },
         * {
         *     pathStartingWith   : "/",
         *     serveFromDirectory : path.resolve(__dirname, "..", "static")
         * }]
         */
        staticPathsMapping? : { pathStartingWith : string, serveFromDirectory : string }[]
        onListening? : (error : boolean) => void;

        /**
         * Sets the "cache-control" response header for responses handled by staticPaths
         */
        cacheControlForStaticPaths? : string;

        /**
         * Callback fired upon new web request received.
         * If this callback returns TRUE then the default responding flow will terminate
         * (so meaning that this callback execution has handled the request and its response),
         * otherwise the responding flow will execute normally returning the static content.
         *
         * @param res
         * @param req
         * @returns {boolean}
         */
        onWebRequest? : (res : uWS.HttpResponse, req : uWS.HttpRequest) => boolean;

        useSSL? : boolean;
        sslKeyFileName? : string;
        sslCertFileName? : string;
    }

    let simpleWebServerSockets = [];

    /**
     * Simply starts a web server serving static resources mapped to one or more directories on file system.
     * Can be used also for dynamic content (ex a small API) using onWebRequest callback
     *
     * @requires  {} NPM Libraries 'uWebSockets.js' and 'mime-types'
     * @param opt
     */
    export function StartSimpleWebServer(opt : ISimpleWebServerOptions) : void
    {
        let uWs : typeof uWS = require('uWebSockets.js');
        let mime = require('mime-types');

        let defaultSimpleWebServerOptions : ISimpleWebServerOptions = {
            port                       : 8989,
            basePath                   : "/",
            staticPathsMapping         : [{pathStartingWith : "/", serveFromDirectory : path.resolve(__dirname)}],
            cacheControlForStaticPaths : "max-age=3600",
            useSSL                     : false
        };

        if (!__.IsNotNullObject(opt))
            opt = __.CloneObj(defaultSimpleWebServerOptions);
        else
            opt = __.MergeObj(defaultSimpleWebServerOptions, opt);
        if (!__.StringStartsWith(opt.basePath, "/"))
            opt.basePath = "/" + opt.basePath;
        if (!__.StringEndsWith(opt.basePath, "/"))
            opt.basePath += "/";

        let onReq = (res : uWS.HttpResponse, req : uWS.HttpRequest) =>
        {
            res.ended = false;
            //We ALWAYS listen to onAborted to handle also ASYNC responses
            res.onAborted(() =>
            {
                try
                {
                    res.end();  /*Can throw "Invalid access of discarded (invalid, deleted) uWS.HttpResponse/SSLHttpResponse"*/
                }
                catch (e)
                { }
                res.ended = true;
            });

            if (__.IsFunction(opt.onWebRequest))
            {
                if (opt.onWebRequest(res, req) == true)
                    return;
            }

            //resolve the file path
            let url = req.getUrl().replace(/\.\./g, "");
            let filePath : string;
            for (let i = 0; i < opt.staticPathsMapping.length; i++)
            {
                let map = opt.staticPathsMapping[i];
                if (__.StringStartsWith(url, map.pathStartingWith, false))
                {
                    filePath = __.EnsureEndsWith(map.serveFromDirectory, GetDirectoriesSeparator());
                    filePath = path.resolve(filePath, url.substr(map.pathStartingWith.length));
                    break;
                }
            }
            if (__.IsNullOrEmpty(filePath))
            {
                if(!res.ended)
                {
                    res.writeStatus("500");
                    res.end("ERROR: server can't resolve your request");
                }
                return;
            }

            //select the right content type
            let lastUrlSegment : string = url.substr(url.lastIndexOf("/"));
            if (lastUrlSegment.indexOf(".") >= 0)
                lastUrlSegment = lastUrlSegment.substr(lastUrlSegment.lastIndexOf("."));

            let contentType = mime.contentType(lastUrlSegment);
            if (contentType === false)
                contentType = 'application/octet-stream';

            let binary = contentType.indexOf("text") < 0 && contentType.indexOf("script") < 0;

            try
            {
                fs.readFile(filePath, binary ? undefined : {encoding : "utf8"}, (err, data : Buffer) =>
                {
                    if(res.ended)
                        return;

                    if (err)
                    {
                        if (__.StringStartsWith(err.message, "ENOENT", false))
                        {
                            res.writeStatus("404");
                            res.end("NOT FOUND");
                        }
                        else
                        {
                            res.writeStatus("500");
                            res.end("ERROR: " + err.message);
                        }
                    }
                    else
                    {
                        res.writeStatus("200");
                        res.writeHeader("content-type", contentType);
                        if (__.IsNotEmptyString(opt.cacheControlForStaticPaths))
                            res.writeHeader("cache-control", opt.cacheControlForStaticPaths);
                        res.end(data);
                    }
                });
            }
            catch (err)
            {
                if(res.ended)
                    return;

                res.writeStatus("500");
                res.end("ERROR: " + err.message);
            }

        };

        let app = opt.useSSL ? "SSLApp" : "App";
        uWs[app](opt.useSSL ? {
            key_file_name  : opt.sslKeyFileName,
            cert_file_name : opt.sslCertFileName,
        } : {})
            .any(opt.basePath + '*', onReq)
            .any(opt.basePath, onReq)
            .listen(opt.port, (listenSocket) =>
            {
                simpleWebServerSockets.push(listenSocket);

                if (__.IsFunction(opt.onListening))
                {
                    opt.onListening(__.IsNull(listenSocket));
                }
            });
    }


    export function StopAllSimpleWebServers() : void
    {
        let uWS = require('uWebSockets.js');

        simpleWebServerSockets.forEach((sock) =>
        {
            try
            {
                uWS.us_listen_socket_close(sock);
            }
            catch (e)
            {}
        });
        simpleWebServerSockets = [];
    }


    /**
     * Helper function for reading posted data
     * @param res
     * @param onReadCompleted Called when all posted data has been read and parsed
     * @param onError Called when an error occurs
     */
    export function ReadRequestBody(res : uWS.HttpResponse, onReadCompleted : (json : any, str : string) => void, onError : () => void) : void
    {
        if(res.ended)
            return;

        let buffer;
        res.onData((ab, isLast) =>
        {
            let chunk = Buffer.from(ab);
            if (isLast)
            {
                let finalStr : string;
                if (buffer)
                {
                    finalStr = Buffer.concat([buffer, chunk]).toString();
                }
                else
                {
                    finalStr = chunk.toString();
                }

                //try to parse received string
                let finalObj = undefined;
                try
                {
                    finalObj = JSON.parse(finalStr);
                }
                catch (e)
                {}

                onReadCompleted(finalObj, finalStr);
            }
            else
            {
                if (buffer)
                {
                    buffer = Buffer.concat([buffer, chunk]);
                }
                else
                {
                    buffer = Buffer.concat([chunk]);
                }
            }
        });

        /* Register error cb */
        res.onAborted(onError);
    }


    /**
     * Helper function for answering a request with json data
     * @param res
     * @param status the HTTP status to respond
     * @param content an eventual JSON data to respond
     */
    export function Respond(res : uWS.HttpResponse, status : number, content? : any) : void
    {
        if(res.ended)
            return;

        res.writeStatus(status.toString());
        res.writeHeader("content-type", "application/json");
        if (__.IsNotNull(content))
            res.end(JSON.stringify(content));
        else if (content === null)
            res.end(JSON.stringify(null));
        else if (content === undefined)
            res.end();
    }


    /**
     * Makes a Server to Server HTTP[S] call and notifies the caller SURELY 1 ONLY TIME with
     * an instance of Error, or otherwise with the body of the response (JSON / text / binary)
     *
     * @param requestOptions
     * @param onCallEnd The callback is called FOR SURE ONLY ONE TIME with
     * an instance of Error, or otherwise with the body of the response (JSON / text) together with its HTTP StatusCode
     */
    export function S2SCall(requestOptions : https.RequestOptions, onCallEnd : (result? : any, httpStatusCode? : number) => void) : void
    {
        if (!__.IsFunction(onCallEnd))
            onCallEnd = () => {};

        let options = __.MergeObj({
            hostname           : "localhost",
            port               : 80,
            protocol           : 'http:',
            path               : "/",
            method             : 'GET',
            timeout            : 2000,
            rejectUnauthorized : true
        }, requestOptions);

        let notified = false;
        let libToCall = options.protocol == "https:" ? https : http;
        let s2sRequest = libToCall.request(options, (s2sResponse : http.IncomingMessage) =>
        {
            let readData = "";

            s2sResponse.setEncoding('utf8');
            s2sResponse.on('data', (data : string) =>
            {
                readData += data;
            });
            s2sResponse.on('error', (e) =>
            {
                if (!notified)
                {
                    notified = true;
                    onCallEnd(new Error(e.message));
                }
            });
            s2sResponse.on('end', () =>
            {
                if (!notified)
                {
                    notified = true;
                    let toNotify = readData;
                    try
                    {
                        toNotify = JSON.parse(readData);
                    }
                    catch (e)
                    {
                    }
                    onCallEnd(toNotify, s2sResponse.statusCode);
                }
            });
        });
        s2sRequest.on('error', function(e)
        {
            if (!notified)
            {
                notified = true;
                onCallEnd(new Error(e.message));
            }
        });
        s2sRequest.on('timeout', () =>
        {
            if (!notified)
            {
                notified = true;
                onCallEnd(new Error("TIMEOUT"));
            }
        });
        s2sRequest.end();
    }


    let configurationManagerInstances : { [id : string] : ConfigurationManager<any> } = {};

    /**
     * Represent a configuration file manager: permits to get/set its values and eventually read/save a JSON file
     */
    export class ConfigurationManager<ConfType extends Object>
    {
        private currentFileName : string = null;
        private currentConfiguration : ConfType = null;
        private defaultConfiguration : ConfType = null;


        constructor(fileName : string, createIfNotFound : boolean = true, defaultConfiguration : ConfType = null)
        {
            let me = this;

            me.currentFileName = fileName;
            me.defaultConfiguration = __.EnsureNotNull(defaultConfiguration);
            me.currentConfiguration = me.defaultConfiguration;

            if (createIfNotFound && !fs.existsSync(fileName))
            {
                me.SaveCurrentConfigurationToFile();
            }

            me.LoadCurrentConfigurationFromFile();
        }


        /**
         * Returns the current "in memory" configuration value
         * WARNING: the returned value is CLONED (so can't change it!)
         */
        public GetCurrentConfiguration() : ConfType
        {
            return __.CloneObj(this.currentConfiguration);
        }


        /**
         * Updates the "in memory" configuration value
         * WARNING: doesn't write files automatically!
         *
         * @param newConfiguration
         */
        public SetCurrentConfiguration(newConfiguration : ConfType)
        {
            this.currentConfiguration = __.CloneObj(newConfiguration);
        }


        public GetCurrentFileName() : string
        {
            return this.currentFileName;
        }


        /**
         * Updates the "in memory" configuration value reading it form file
         */
        public LoadCurrentConfigurationFromFile() : void
        {
            let json = fs.readFileSync(this.currentFileName, {encoding : 'utf8'});
            let read = JSON.parse(json);
            this.currentConfiguration = __.MergeObj(this.defaultConfiguration, read);
        }


        /**
         * Saves to the file the current "in memory" configuration value
         */
        public SaveCurrentConfigurationToFile() : void
        {
            fs.writeFileSync(this.currentFileName, JSON.stringify(this.currentConfiguration, null, 4), {encoding : 'utf8'});
        }


        /**
         * Sets a "default manager instance" for a particular "topic/argument".
         * Ex. use case:
         * At app start we create a manager instance and call SetManagerForTopic with "MainConf" topic.
         * All other modules needing it call ConfigurationManager.GetManagerForTopic to obtain it.
         */
        static SetManagerForTopic<T>(topic : string, manager : ConfigurationManager<T>) : void
        {
            configurationManagerInstances[topic] = manager;
        }


        /**
         * Returns the "default manager instance" for a particular "topic/argument", if ever set
         */
        static GetManagerForTopic<T>(topic : string) : ConfigurationManager<T>
        {
            return configurationManagerInstances[topic];
        }

    }



    export module ServerResources
    {
        let currentLanguage : string = "en";
        let allResources : { [id : string] : any } = {};
        let availableLanguages : string[] = ["en", "it"];


        /**
         * Loads in memory EVERY available LANGUAGE from files.
         */
        export function LoadResourcesFiles(rootPath : string, fileTemplate : string = "res_%s.json") : void
        {
            let langs = GetAvailableLanguages();
            langs.forEach((language : string) =>
            {
                let fileName = path.resolve(rootPath, __.Format(fileTemplate, language));

                if (!fs.existsSync(fileName))
                {
                    throw new Error(__.Format("Language \"%s\" not found (file \"%s\")", language, fileName));
                }
                let json = fs.readFileSync(fileName, {encoding : 'utf8'});
                allResources[language] = JSON.parse(json);
            });

            if (langs.length)
                SetCurrentLanguage(langs[0]);       //initially a default one
        }


        /**
         * Sets the current language for the SERVER
         */
        export function SetCurrentLanguage(defaultServerLanguageToSet : string) : void
        {
            if (!IsValidLanguage(defaultServerLanguageToSet))
                throw new Error(__.Format("Language \"%s\" not supported", defaultServerLanguageToSet));

            currentLanguage = defaultServerLanguageToSet;
        }


        /**
         * Gets the current language for the SERVER
         */
        export function GetCurrentLanguage() : string
        {
            return currentLanguage;
        }


        /**
         * Returns a resources dictionary.
         * Returns the specified language (if supported) or the default one
         */
        export function GetResources<T>(forceLanguage : string = null) : T
        {
            if (__.IsEmptyString(forceLanguage))
                forceLanguage = currentLanguage;
            if (!IsValidLanguage(forceLanguage))
                throw new Error(__.Format("Language \"%s\" not supported", forceLanguage));

            return allResources[forceLanguage];
        }


        export function IsValidLanguage(lang : string) : boolean
        {
            return __.SearchInArray(GetAvailableLanguages(), lang) >= 0;
        }


        export function GetAvailableLanguages() : string[]
        {
            return __.CloneObj(availableLanguages);
        }


        export function SetAvailableLanguages(languages : string[]) : void
        {
            availableLanguages = languages;
        }

    }

}

