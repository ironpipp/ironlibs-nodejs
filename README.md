# IronLibs

Ironpipp's useful utilities for both nodejs, browser with jQuery or Angular. 

This repo holds nodejs-only utilities.

## Features
- Fully typed
- by default no dependencies are needed. Are required ONLY when used 
    - **sanitize-filename** for IronLibsNodeJs.SanitizeFileName()
    - **lzma**              for IronLibsNodeJs.LzmaCompress()
    - **uWebSockets.js**    for IronLibsNodeJs.StartSimpleWebServer()  (ex   "uWebSockets.js": "github:uNetworking/uWebSockets.js#v20.6.0")
    - **mime-types**        for IronLibsNodeJs.StartSimpleWebServer()
    - **express**           for IronLibsExpressMVC
    - **sqlite3**           for IronLibsSqlDb
    - **puppeteer-core**    for IronLibsPuppeteer

So install proper packages based on your necessities. 

   
## How to build and test
     > npm run build


## uWebSockets.js (To build uWebSockets.js for ARM 32 bit)

You need the correct ".node" binary according to arch and V8 version.

Ex. in windows with node 15.12.0 file uws_win32_x64_88.node is needed.

In raspberry (arm 32bit)  with node 15.12.0 file uws_linux_arm_88.node is needed.

Unfortunately (1) this is not shipped in npm releases, se we need to build it.

Unfortunately (2) also the build script shipped in git sources doesn't support 32 bit anymore.

This guy (https://github.com/jmscreation/RBPI.uWebSockets.js/)is mantaining a build script for ARM and 32 bit


    - download https://github.com/jmscreation/RBPI.uWebSockets.js/blob/master/build.c
    - edit adding only the node headers of your desired version
    - run gcc build.c
    - execute the binary


this will download ALL the sources needed and build the right binary to be added to the node_modules/uWebSockets.js folder of your project
