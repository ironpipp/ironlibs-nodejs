"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IronLibsPuppeteer = void 0;
var IronLibsPuppeteer;
(function (IronLibsPuppeteer) {
    async function GetInnerText(page, el) {
        return await page.evaluate(elm => { return (elm.textContent); }, el);
    }
    IronLibsPuppeteer.GetInnerText = GetInnerText;
    async function GetInnerHtml(page, el) {
        return await page.evaluate(elm => { return (elm.innerHTML); }, el);
    }
    IronLibsPuppeteer.GetInnerHtml = GetInnerHtml;
})(IronLibsPuppeteer = exports.IronLibsPuppeteer || (exports.IronLibsPuppeteer = {}));
//# sourceMappingURL=puppeteer.js.map