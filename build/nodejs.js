"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IronLibsNodeJs = void 0;
const https = require("https");
const http = require("http");
const fs = require("fs");
const path = require("path");
var IronLibsNodeJs;
(function (IronLibsNodeJs) {
    let __ = require("ironlibs-base/build/common").IronLibsCommon; //using ": typeof IronLibsCommon" we get the same as "import {IronLibsCommon as __} from './common';"
    function IsUnixEnvironment() {
        return process.platform != "win32";
    }
    IronLibsNodeJs.IsUnixEnvironment = IsUnixEnvironment;
    function GetDirectoriesSeparator() {
        return IsUnixEnvironment() ? "/" : "\\";
    }
    IronLibsNodeJs.GetDirectoriesSeparator = GetDirectoriesSeparator;
    /**
     * Returns the APPLICATION DATA directory with the ending separator (ex: "/")
     */
    function GetDataDirectory(appName, ensureExists = true) {
        let ret;
        let sep = GetDirectoriesSeparator();
        if (__.IsNotEmptyString(process.env.APPDATA))
            ret = __.EnsureEndsWith(process.env.APPDATA, sep) + (__.IsNotEmptyString(appName) ? appName + sep : "");
        else
            ret = __.EnsureEndsWith(process.cwd(), sep) + (__.IsNotEmptyString(appName) ? appName + sep : "");
        if (ensureExists) {
            if (!fs.existsSync(ret))
                fs.mkdirSync(ret);
        }
        return ret;
    }
    IronLibsNodeJs.GetDataDirectory = GetDataDirectory;
    /**
     * Reads package.json and ensures all required modules are installed. Will exit if one or more is not found.
     */
    function CheckModules() {
        let pack;
        try {
            pack = require("../package");
        }
        catch (e) {
            console.log("ERROR: Could not load package.json from project root. This file is required for reading project properties.");
            process.exit(1);
        }
        let moduleExists = function (modName) {
            try {
                return require.resolve(modName);
            }
            catch (e) {
                return false;
            }
        };
        let isModuleMissing = false;
        for (let key in pack["dependencies"]) {
            if (!moduleExists(key)) {
                isModuleMissing = true;
                console.log("ERROR: Missing module '" + key + "'");
            }
        }
        if (isModuleMissing) {
            console.log("ERROR: Required modules are not installed. Run 'npm install' from command line.");
            process.exit(1);
        }
        delete require.cache[pack];
    }
    IronLibsNodeJs.CheckModules = CheckModules;
    /**
     * Converts an ArrayBuffer to an UTF8 string
     */
    function ArrayBufferToUtf8String(buff) {
        let uInt8Array = new Uint8Array(buff);
        let i = uInt8Array.length;
        let binaryString = new Array(i);
        while (i--) {
            binaryString[i] = String.fromCharCode(uInt8Array[i]);
        }
        return binaryString.join(''); //WARNING: returns UTF-8 string!!!
    }
    IronLibsNodeJs.ArrayBufferToUtf8String = ArrayBufferToUtf8String;
    /**
     * Return the BASE64 string of the SHA1 hash of the passed string
     */
    function GetHash(toBeHashed) {
        let crypto = require('crypto');
        toBeHashed = __.EnsureString(toBeHashed, "");
        let shasum = crypto.createHash('sha1');
        shasum.update(toBeHashed);
        return shasum.digest('base64');
    }
    IronLibsNodeJs.GetHash = GetHash;
    let sanitizeFilePath = null;
    /**
     * Accepts only a FILE name (with extension, NO path segments).
     * Returns a valid one.
     * Requires "sanitize-filename" package
     */
    function SanitizeFileName(fileName, replaceInvalidCharsWith = "") {
        if (__.IsNull(sanitizeFilePath))
            sanitizeFilePath = require("sanitize-filename");
        if (!__.IsString(replaceInvalidCharsWith))
            replaceInvalidCharsWith = "";
        return sanitizeFilePath(fileName, { replacement: replaceInvalidCharsWith });
    }
    IronLibsNodeJs.SanitizeFileName = SanitizeFileName;
    /**
     * The SANITIZED paths tree will be returned (without file!). Il will always end with pathSegmentsSeparator.
     * Requires "sanitize-filename" package
     */
    function SanitizeFolderPath(fullPath, replaceInvalidCharsWith = "", pathSegmentsSeparator = GetDirectoriesSeparator()) {
        if (__.StringEndsWith(fullPath, pathSegmentsSeparator))
            fullPath = fullPath.substr(0, fullPath.length - pathSegmentsSeparator.length);
        let segments = fullPath.split(pathSegmentsSeparator);
        let ret = segments[0] + pathSegmentsSeparator;
        for (let i = 1; i < segments.length; i++) {
            segments[i] = SanitizeFileName(segments[i], replaceInvalidCharsWith);
            ret = ret + segments[i] + pathSegmentsSeparator;
        }
        return ret;
    }
    IronLibsNodeJs.SanitizeFolderPath = SanitizeFolderPath;
    /**
     * Ensures that all subdirectories exist. If one or more are missing creates them.
     *
     * fullPath MUST NOT point to a file, ONLY DIRECTORIES!
     * fullPath CAN contain invalid characters. But only its SANITIZED version is created and returned.
     */
    function EnsureSanitizedPathsTreeIsCreated(fullPath, replaceInvalidCharsWith = "") {
        let separator = GetDirectoriesSeparator();
        let segments = fullPath.split(separator);
        let ret = segments[0] + separator;
        for (let i = 1; i < segments.length; i++) {
            segments[i] = SanitizeFileName(segments[i], replaceInvalidCharsWith);
            ret = ret + segments[i] + separator;
            if (!fs.existsSync(ret))
                fs.mkdirSync(ret);
        }
        return ret;
    }
    IronLibsNodeJs.EnsureSanitizedPathsTreeIsCreated = EnsureSanitizedPathsTreeIsCreated;
    /**
     * Checks if the passed string is an absolute path (valid for both UNIX and Windows)
     * Examples of absolute paths: \    \asd\qwe    /asd/qwe    d:\  c:\asd\qwe
     * @param s
     */
    function IsAbsolutePath(s) {
        return (s.length > 0 && (s[0] == "/" || s[0] == "\\")) || //ex:  \    \asd\qwe    /asd/qwe
            (s.length > 2 && s[1] == ":" && s[2] == "\\"); //ex:  d:\  c:\qwe
    }
    IronLibsNodeJs.IsAbsolutePath = IsAbsolutePath;
    function CopyFile(sourceFile, destFile, onEnd = null, deleteSourceAfterSuccessfulCopy = false) {
        if (!__.IsFunction(onEnd))
            onEnd = function () { };
        let is = fs.createReadStream(sourceFile);
        let os = fs.createWriteStream(destFile);
        is.on('error', onEnd);
        os.on('error', onEnd);
        is.pipe(os);
        is.on('end', function () {
            is.close();
            is.destroy();
            os.close();
            os.destroy();
        });
        is.on('close', function () {
            if (deleteSourceAfterSuccessfulCopy)
                fs.unlink(sourceFile, onEnd);
            else
                onEnd(null);
        });
    }
    IronLibsNodeJs.CopyFile = CopyFile;
    /**
     * Converts an ArrayBuffer to an UTF8 string
     */
    function ArrayBufferToBlob(buff) {
        let uInt8Array = new Uint8Array(buff);
        let i = uInt8Array.length;
        let binaryString = new Array(i);
        while (i--) {
            binaryString[i] = String.fromCharCode(uInt8Array[i]);
        }
        return binaryString.join(''); //WARNING: returns UTF-8 string!!!
    }
    IronLibsNodeJs.ArrayBufferToBlob = ArrayBufferToBlob;
    function OpenWithExplorer(path) {
        let cp = require('child_process');
        let exec = cp.exec;
        exec('start "" "' + SanitizeFolderPath(path) + '"');
    }
    IronLibsNodeJs.OpenWithExplorer = OpenWithExplorer;
    /**
     *  Compresses passed data to a valid .7z archive (contains only ONE file named "compressed")
     *  Requires "lzma" package
     *
     * @param dataToCompress The data to compress in any form
     * @param compressionLevel from 1 (fastest) to 9 (best)
     * @param onEnd The "compressedData" param can be converted to a typed array calling "new Uint8Array(compressedData)". The "error" param is 0 upon success or the Error object otherwise
     * @param onProgress The "progress" param goes from 0 to 1
     */
    function LzmaCompress(dataToCompress, compressionLevel, onEnd, onProgress) {
        let lzma = require("lzma");
        lzma.compress(dataToCompress, compressionLevel, onEnd, onProgress);
    }
    IronLibsNodeJs.LzmaCompress = LzmaCompress;
    /**
     *  Decompresses back the passed compressed data
     *  Requires "lzma" package
     *
     * @param compressedData The binary data to decompress
     * @param onEnd The "decompressedData" param is a string if it decodes as valid UTF-8 text, otherwise will be a Uint8Array instance. The "error" param is 0 upon success or the Error object otherwise
     * @param onProgress The "progress" param goes from 0 to 1
     */
    function LzmaDecompress(compressedData, onEnd, onProgress) {
        let lzma = require("lzma");
        lzma.decompress(compressedData, onEnd, onProgress);
    }
    IronLibsNodeJs.LzmaDecompress = LzmaDecompress;
    let simpleWebServerSockets = [];
    /**
     * Simply starts a web server serving static resources mapped to one or more directories on file system.
     * Can be used also for dynamic content (ex a small API) using onWebRequest callback
     *
     * @requires  {} NPM Libraries 'uWebSockets.js' and 'mime-types'
     * @param opt
     */
    function StartSimpleWebServer(opt) {
        let uWs = require('uWebSockets.js');
        let mime = require('mime-types');
        let defaultSimpleWebServerOptions = {
            port: 8989,
            basePath: "/",
            staticPathsMapping: [{ pathStartingWith: "/", serveFromDirectory: path.resolve(__dirname) }],
            cacheControlForStaticPaths: "max-age=3600",
            useSSL: false
        };
        if (!__.IsNotNullObject(opt))
            opt = __.CloneObj(defaultSimpleWebServerOptions);
        else
            opt = __.MergeObj(defaultSimpleWebServerOptions, opt);
        if (!__.StringStartsWith(opt.basePath, "/"))
            opt.basePath = "/" + opt.basePath;
        if (!__.StringEndsWith(opt.basePath, "/"))
            opt.basePath += "/";
        let onReq = (res, req) => {
            res.ended = false;
            //We ALWAYS listen to onAborted to handle also ASYNC responses
            res.onAborted(() => {
                try {
                    res.end(); /*Can throw "Invalid access of discarded (invalid, deleted) uWS.HttpResponse/SSLHttpResponse"*/
                }
                catch (e) { }
                res.ended = true;
            });
            if (__.IsFunction(opt.onWebRequest)) {
                if (opt.onWebRequest(res, req) == true)
                    return;
            }
            //resolve the file path
            let url = req.getUrl().replace(/\.\./g, "");
            let filePath;
            for (let i = 0; i < opt.staticPathsMapping.length; i++) {
                let map = opt.staticPathsMapping[i];
                if (__.StringStartsWith(url, map.pathStartingWith, false)) {
                    filePath = __.EnsureEndsWith(map.serveFromDirectory, GetDirectoriesSeparator());
                    filePath = path.resolve(filePath, url.substr(map.pathStartingWith.length));
                    break;
                }
            }
            if (__.IsNullOrEmpty(filePath)) {
                if (!res.ended) {
                    res.writeStatus("500");
                    res.end("ERROR: server can't resolve your request");
                }
                return;
            }
            //select the right content type
            let lastUrlSegment = url.substr(url.lastIndexOf("/"));
            if (lastUrlSegment.indexOf(".") >= 0)
                lastUrlSegment = lastUrlSegment.substr(lastUrlSegment.lastIndexOf("."));
            let contentType = mime.contentType(lastUrlSegment);
            if (contentType === false)
                contentType = 'application/octet-stream';
            let binary = contentType.indexOf("text") < 0 && contentType.indexOf("script") < 0;
            try {
                fs.readFile(filePath, binary ? undefined : { encoding: "utf8" }, (err, data) => {
                    if (res.ended)
                        return;
                    if (err) {
                        if (__.StringStartsWith(err.message, "ENOENT", false)) {
                            res.writeStatus("404");
                            res.end("NOT FOUND");
                        }
                        else {
                            res.writeStatus("500");
                            res.end("ERROR: " + err.message);
                        }
                    }
                    else {
                        res.writeStatus("200");
                        res.writeHeader("content-type", contentType);
                        if (__.IsNotEmptyString(opt.cacheControlForStaticPaths))
                            res.writeHeader("cache-control", opt.cacheControlForStaticPaths);
                        res.end(data);
                    }
                });
            }
            catch (err) {
                if (res.ended)
                    return;
                res.writeStatus("500");
                res.end("ERROR: " + err.message);
            }
        };
        let app = opt.useSSL ? "SSLApp" : "App";
        uWs[app](opt.useSSL ? {
            key_file_name: opt.sslKeyFileName,
            cert_file_name: opt.sslCertFileName,
        } : {})
            .any(opt.basePath + '*', onReq)
            .any(opt.basePath, onReq)
            .listen(opt.port, (listenSocket) => {
            simpleWebServerSockets.push(listenSocket);
            if (__.IsFunction(opt.onListening)) {
                opt.onListening(__.IsNull(listenSocket));
            }
        });
    }
    IronLibsNodeJs.StartSimpleWebServer = StartSimpleWebServer;
    function StopAllSimpleWebServers() {
        let uWS = require('uWebSockets.js');
        simpleWebServerSockets.forEach((sock) => {
            try {
                uWS.us_listen_socket_close(sock);
            }
            catch (e) { }
        });
        simpleWebServerSockets = [];
    }
    IronLibsNodeJs.StopAllSimpleWebServers = StopAllSimpleWebServers;
    /**
     * Helper function for reading posted data
     * @param res
     * @param onReadCompleted Called when all posted data has been read and parsed
     * @param onError Called when an error occurs
     */
    function ReadRequestBody(res, onReadCompleted, onError) {
        if (res.ended)
            return;
        let buffer;
        res.onData((ab, isLast) => {
            let chunk = Buffer.from(ab);
            if (isLast) {
                let finalStr;
                if (buffer) {
                    finalStr = Buffer.concat([buffer, chunk]).toString();
                }
                else {
                    finalStr = chunk.toString();
                }
                //try to parse received string
                let finalObj = undefined;
                try {
                    finalObj = JSON.parse(finalStr);
                }
                catch (e) { }
                onReadCompleted(finalObj, finalStr);
            }
            else {
                if (buffer) {
                    buffer = Buffer.concat([buffer, chunk]);
                }
                else {
                    buffer = Buffer.concat([chunk]);
                }
            }
        });
        /* Register error cb */
        res.onAborted(onError);
    }
    IronLibsNodeJs.ReadRequestBody = ReadRequestBody;
    /**
     * Helper function for answering a request with json data
     * @param res
     * @param status the HTTP status to respond
     * @param content an eventual JSON data to respond
     */
    function Respond(res, status, content) {
        if (res.ended)
            return;
        res.writeStatus(status.toString());
        res.writeHeader("content-type", "application/json");
        if (__.IsNotNull(content))
            res.end(JSON.stringify(content));
        else if (content === null)
            res.end(JSON.stringify(null));
        else if (content === undefined)
            res.end();
    }
    IronLibsNodeJs.Respond = Respond;
    /**
     * Makes a Server to Server HTTP[S] call and notifies the caller SURELY 1 ONLY TIME with
     * an instance of Error, or otherwise with the body of the response (JSON / text / binary)
     *
     * @param requestOptions
     * @param onCallEnd The callback is called FOR SURE ONLY ONE TIME with
     * an instance of Error, or otherwise with the body of the response (JSON / text) together with its HTTP StatusCode
     */
    function S2SCall(requestOptions, onCallEnd) {
        if (!__.IsFunction(onCallEnd))
            onCallEnd = () => { };
        let options = __.MergeObj({
            hostname: "localhost",
            port: 80,
            protocol: 'http:',
            path: "/",
            method: 'GET',
            timeout: 2000,
            rejectUnauthorized: true
        }, requestOptions);
        let notified = false;
        let libToCall = options.protocol == "https:" ? https : http;
        let s2sRequest = libToCall.request(options, (s2sResponse) => {
            let readData = "";
            s2sResponse.setEncoding('utf8');
            s2sResponse.on('data', (data) => {
                readData += data;
            });
            s2sResponse.on('error', (e) => {
                if (!notified) {
                    notified = true;
                    onCallEnd(new Error(e.message));
                }
            });
            s2sResponse.on('end', () => {
                if (!notified) {
                    notified = true;
                    let toNotify = readData;
                    try {
                        toNotify = JSON.parse(readData);
                    }
                    catch (e) {
                    }
                    onCallEnd(toNotify, s2sResponse.statusCode);
                }
            });
        });
        s2sRequest.on('error', function (e) {
            if (!notified) {
                notified = true;
                onCallEnd(new Error(e.message));
            }
        });
        s2sRequest.on('timeout', () => {
            if (!notified) {
                notified = true;
                onCallEnd(new Error("TIMEOUT"));
            }
        });
        s2sRequest.end();
    }
    IronLibsNodeJs.S2SCall = S2SCall;
    let configurationManagerInstances = {};
    /**
     * Represent a configuration file manager: permits to get/set its values and eventually read/save a JSON file
     */
    class ConfigurationManager {
        currentFileName = null;
        currentConfiguration = null;
        defaultConfiguration = null;
        constructor(fileName, createIfNotFound = true, defaultConfiguration = null) {
            let me = this;
            me.currentFileName = fileName;
            me.defaultConfiguration = __.EnsureNotNull(defaultConfiguration);
            me.currentConfiguration = me.defaultConfiguration;
            if (createIfNotFound && !fs.existsSync(fileName)) {
                me.SaveCurrentConfigurationToFile();
            }
            me.LoadCurrentConfigurationFromFile();
        }
        /**
         * Returns the current "in memory" configuration value
         * WARNING: the returned value is CLONED (so can't change it!)
         */
        GetCurrentConfiguration() {
            return __.CloneObj(this.currentConfiguration);
        }
        /**
         * Updates the "in memory" configuration value
         * WARNING: doesn't write files automatically!
         *
         * @param newConfiguration
         */
        SetCurrentConfiguration(newConfiguration) {
            this.currentConfiguration = __.CloneObj(newConfiguration);
        }
        GetCurrentFileName() {
            return this.currentFileName;
        }
        /**
         * Updates the "in memory" configuration value reading it form file
         */
        LoadCurrentConfigurationFromFile() {
            let json = fs.readFileSync(this.currentFileName, { encoding: 'utf8' });
            let read = JSON.parse(json);
            this.currentConfiguration = __.MergeObj(this.defaultConfiguration, read);
        }
        /**
         * Saves to the file the current "in memory" configuration value
         */
        SaveCurrentConfigurationToFile() {
            fs.writeFileSync(this.currentFileName, JSON.stringify(this.currentConfiguration, null, 4), { encoding: 'utf8' });
        }
        /**
         * Sets a "default manager instance" for a particular "topic/argument".
         * Ex. use case:
         * At app start we create a manager instance and call SetManagerForTopic with "MainConf" topic.
         * All other modules needing it call ConfigurationManager.GetManagerForTopic to obtain it.
         */
        static SetManagerForTopic(topic, manager) {
            configurationManagerInstances[topic] = manager;
        }
        /**
         * Returns the "default manager instance" for a particular "topic/argument", if ever set
         */
        static GetManagerForTopic(topic) {
            return configurationManagerInstances[topic];
        }
    }
    IronLibsNodeJs.ConfigurationManager = ConfigurationManager;
    let ServerResources;
    (function (ServerResources) {
        let currentLanguage = "en";
        let allResources = {};
        let availableLanguages = ["en", "it"];
        /**
         * Loads in memory EVERY available LANGUAGE from files.
         */
        function LoadResourcesFiles(rootPath, fileTemplate = "res_%s.json") {
            let langs = GetAvailableLanguages();
            langs.forEach((language) => {
                let fileName = path.resolve(rootPath, __.Format(fileTemplate, language));
                if (!fs.existsSync(fileName)) {
                    throw new Error(__.Format("Language \"%s\" not found (file \"%s\")", language, fileName));
                }
                let json = fs.readFileSync(fileName, { encoding: 'utf8' });
                allResources[language] = JSON.parse(json);
            });
            if (langs.length)
                SetCurrentLanguage(langs[0]); //initially a default one
        }
        ServerResources.LoadResourcesFiles = LoadResourcesFiles;
        /**
         * Sets the current language for the SERVER
         */
        function SetCurrentLanguage(defaultServerLanguageToSet) {
            if (!IsValidLanguage(defaultServerLanguageToSet))
                throw new Error(__.Format("Language \"%s\" not supported", defaultServerLanguageToSet));
            currentLanguage = defaultServerLanguageToSet;
        }
        ServerResources.SetCurrentLanguage = SetCurrentLanguage;
        /**
         * Gets the current language for the SERVER
         */
        function GetCurrentLanguage() {
            return currentLanguage;
        }
        ServerResources.GetCurrentLanguage = GetCurrentLanguage;
        /**
         * Returns a resources dictionary.
         * Returns the specified language (if supported) or the default one
         */
        function GetResources(forceLanguage = null) {
            if (__.IsEmptyString(forceLanguage))
                forceLanguage = currentLanguage;
            if (!IsValidLanguage(forceLanguage))
                throw new Error(__.Format("Language \"%s\" not supported", forceLanguage));
            return allResources[forceLanguage];
        }
        ServerResources.GetResources = GetResources;
        function IsValidLanguage(lang) {
            return __.SearchInArray(GetAvailableLanguages(), lang) >= 0;
        }
        ServerResources.IsValidLanguage = IsValidLanguage;
        function GetAvailableLanguages() {
            return __.CloneObj(availableLanguages);
        }
        ServerResources.GetAvailableLanguages = GetAvailableLanguages;
        function SetAvailableLanguages(languages) {
            availableLanguages = languages;
        }
        ServerResources.SetAvailableLanguages = SetAvailableLanguages;
    })(ServerResources = IronLibsNodeJs.ServerResources || (IronLibsNodeJs.ServerResources = {}));
})(IronLibsNodeJs = exports.IronLibsNodeJs || (exports.IronLibsNodeJs = {}));
//# sourceMappingURL=nodejs.js.map