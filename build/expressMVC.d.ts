import express = require('express');
export declare module IronLibsExpressMVC {
    enum NotificationType {
        ERROR = 0,
        SUCCESS = 1,
        INFO = 2
    }
    /**
     * The data that normally travels in the net in ajax calls
     */
    interface IServerResponseModel {
        IsValid: boolean;
        Notifications: {
            MessageText: string;
            PropertyName?: string;
            Type: NotificationType;
        }[];
        Data?: any;
    }
    class ControllerBase {
        constructor();
        private readonly router;
        GetRouter(): express.Router;
        /**
         * Registers a controller action invoked for ANY http verb
         *
         * @param url one or more urls (also with wildcards) to be matched in order to call the callback
         * @param clbk The action executed upon an url match
         */
        RegisterActionAll(url: string | string[], clbk: express.RequestHandler): void;
        /**
         * Registers a controller action invoked for GET http verb
         *
         * @param url one or more urls (also with wildcards) to be matched in order to call the callback
         * @param clbk The action executed upon an url match
         */
        RegisterActionGet(url: string | string[], clbk: express.RequestHandler): void;
        /**
         * Registers a controller action invoked for POST http verb
         *
         * @param url one or more urls (also with wildcards) to be matched in order to call the callback
         * @param clbk The action executed upon an url match
         */
        RegisterActionPost(url: string | string[], clbk: express.RequestHandler): void;
        Respond(res: express.Response, msg: string | IServerResponseModel, statusCode?: number, headerValues?: object): void;
        RespondOk(res: express.Response, data?: any, notification?: string, type?: NotificationType): void;
        RespondError(res: express.Response, data?: any, notification?: string, clientError?: boolean): void;
        RespondRedirect(res: express.Response, where: string): void;
    }
}
