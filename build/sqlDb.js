"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IronLibsSqlDb = void 0;
const fs = require("fs");
const sqlite3 = require("sqlite3");
var IronLibsSqlDb;
(function (IronLibsSqlDb) {
    let __ = require("ironlibs-base/build/common").IronLibsCommon; //using ": typeof IronLibsCommon" we get the same as "import {IronLibsCommon as __} from './common';"
    let DB_FIELD_TYPE;
    (function (DB_FIELD_TYPE) {
        DB_FIELD_TYPE[DB_FIELD_TYPE["TEXT"] = 0] = "TEXT";
        DB_FIELD_TYPE[DB_FIELD_TYPE["NUMBER_INTEGER"] = 1] = "NUMBER_INTEGER";
        DB_FIELD_TYPE[DB_FIELD_TYPE["NUMBER_DOUBLE"] = 2] = "NUMBER_DOUBLE";
        DB_FIELD_TYPE[DB_FIELD_TYPE["DATE"] = 3] = "DATE";
        DB_FIELD_TYPE[DB_FIELD_TYPE["BINARY"] = 4] = "BINARY";
    })(DB_FIELD_TYPE = IronLibsSqlDb.DB_FIELD_TYPE || (IronLibsSqlDb.DB_FIELD_TYPE = {}));
    let LOAD_DUMP_BEHAVIOUR;
    (function (LOAD_DUMP_BEHAVIOUR) {
        LOAD_DUMP_BEHAVIOUR[LOAD_DUMP_BEHAVIOUR["INSERTBULK"] = 0] = "INSERTBULK";
        LOAD_DUMP_BEHAVIOUR[LOAD_DUMP_BEHAVIOUR["UPDATEIFFOUND"] = 1] = "UPDATEIFFOUND";
        LOAD_DUMP_BEHAVIOUR[LOAD_DUMP_BEHAVIOUR["DELETEALLDATA"] = 2] = "DELETEALLDATA";
    })(LOAD_DUMP_BEHAVIOUR = IronLibsSqlDb.LOAD_DUMP_BEHAVIOUR || (IronLibsSqlDb.LOAD_DUMP_BEHAVIOUR = {}));
    class Entity {
        constructor() { }
        SqlTableName() {
            return "Entity";
        }
        SqlTableFields() {
            return [];
        }
        SqlTableFieldNames() {
            return this.SqlTableFields().map(function (el) { return el.name; });
        }
    }
    IronLibsSqlDb.Entity = Entity;
    class IdEntity extends Entity {
        Id;
        SqlTableName() {
            return "IdEntity";
        }
        SqlTableFields() {
            return [
                { name: "Id", type: DB_FIELD_TYPE.TEXT, primary: true },
            ];
        }
    }
    IronLibsSqlDb.IdEntity = IdEntity;
    class EntityCollection {
        tconstructor;
        fkName;
        values;
        isLoaded;
        constructor(tconstructor, fkName, values = [], isLoaded = false) {
            this.tconstructor = tconstructor;
            this.fkName = fkName;
            this.values = values;
            this.isLoaded = isLoaded;
            if (__.IsArray(values))
                this.values = values;
            else
                this.values = [];
        }
        Values() {
            return this.values;
        }
        IsLoaded() {
            return this.isLoaded;
        }
        FkName() {
            return this.fkName;
        }
        Tconstructor() {
            return this.tconstructor;
        }
        LoadWithValues(values) {
            let me = this;
            me.values = [];
            values.forEach(function (el) {
                me.values.push(el);
            });
            me.isLoaded = true;
        }
    }
    IronLibsSqlDb.EntityCollection = EntityCollection;
    /*#################################################################################################################*/
    /*#################################################################################################################*/
    class DB {
        db;
        opt;
        instanceId;
        lastSuccessfulDump = null;
        automaticDumpTimer = null;
        automaticDumpPaused = false;
        automaticDumpIsProcessing = false;
        isClosed = false;
        /**
         *
         * @param options Sets the following possible options:
         *        fileName : Specifies the filename of the stored database file, or when inMemoryMode == true specifies the filename of the dump file.
         *        verbose : Specifies whether to log SQL statements or not.
         *        loggingFunction : Specifies the logging function accepting the string to log.
         *        inMemoryMode : If FALSE data is stored immediately (BAD queries performances, especially for non-SSD hard disks).
         *                       If TRUE data is kept in memory and persisted only in intervals of inMemoryDumpEveryMs milliseconds.
         *        inMemoryDumpEveryMs : Has sense only when inMemoryMode == true.
         *                              If > 0  data is persisted automatically as a dump every the specified interval in milliseconds.
         *                              If <= 0 data is NEVER persisted automatically.
         *        inMemoryDumpOnlyIfDirty : If TRUE the automatic dump is updated only if data has changed in any repository.
         *                                  If FALSE the dump is always updated.
         *        inMemoryRepositoriesToDump : The repositories to use to dump data. WARNING: only data of these repositories will be persisted automatically!
         */
        constructor(options = {}) {
            let me = this;
            me.opt = __.MergeObj(DB.DefaultOptions, options);
            me.instanceId = __.Guid.New();
            //OPTIONS ERRORS CHECK
            if (me.opt.inMemoryMode && !__.IsArray(me.opt.inMemoryEntitiesToDump))
                throw new Error("When inMemoryMode == true then inMemoryRepositoriesToDump must be a valid array");
            if (me.opt.verbose && !__.IsFunction(me.opt.loggingFunction))
                throw new Error("When verbose == true then loggingFunction must be a valid function");
            me.db = new sqlite3.Database(me.opt.inMemoryMode ? ":memory:" : me.opt.fileName);
            //Start automatic dump?
            if (me.opt.inMemoryMode && me.opt.inMemoryDumpEveryMs > 0) {
                me.automaticDumpTimer = setInterval(function () {
                    if (me.automaticDumpPaused || me.automaticDumpIsProcessing)
                        return;
                    me.automaticDumpIsProcessing = true;
                    me.PerformDump(me.opt.fileName, me.opt.inMemoryEntitiesToDump, me.opt.inMemoryDumpOnlyIfDirty);
                }, me.opt.inMemoryDumpEveryMs);
                if (me.opt.verbose)
                    me.opt.loggingFunction("[DB " + me.opt.fileName + "] Started automatic dump every " + me.opt.inMemoryDumpEveryMs + "ms");
            }
        }
        GetInstanceId() {
            return this.instanceId;
        }
        GetIsVerbose() {
            return this.opt.verbose;
        }
        GetInMemoryEntitiesToDump() {
            return this.opt.inMemoryEntitiesToDump;
        }
        GetLoggingFunction() {
            return this.opt.loggingFunction;
        }
        GetFileName() {
            return this.opt.fileName;
        }
        GetLastSuccessfulDump() {
            return this.lastSuccessfulDump;
        }
        ExecuteSelect(sql, onEnd) {
            let me = this;
            let ret = [];
            if (this.opt.verbose)
                this.opt.loggingFunction("[DB " + this.opt.fileName + "] Execute SELECT: " + sql);
            me.db.each(sql, function (err, row) {
                ret.push(row);
            }, function (err, count) {
                if (__.IsFunction(onEnd))
                    onEnd(ret, err);
            });
        }
        ExecuteSql(sql, callback) {
            if (this.opt.verbose)
                this.opt.loggingFunction("[DB " + this.opt.fileName + "] Execute SQL: " + sql);
            this.db.run(sql, callback);
        }
        Transaction(callback, serial = true) {
            if (serial)
                this.db.serialize(callback);
            else
                this.db.parallelize(callback);
        }
        /**
         * Pauses an already started automatic dump
         */
        PauseAutomaticDump() {
            this.automaticDumpPaused = true;
        }
        /**
         * Pauses an already paused automatic dump
         */
        RestoreAutomaticDump() {
            this.automaticDumpPaused = false;
        }
        /**
         * Executes a dump independently from the automatic system.
         *
         * @param fileName Specify the file to be saved. If NULL then options.fileName will be used
         * @param specificEntitiesToDump Specify the entities types to dump. If NULL then options.inMemoryEntitiesToDump will be used
         * @param onEnd Called with non-null "err" in case of error
         */
        ForceDump(fileName, specificEntitiesToDump, onEnd) {
            if (!__.IsArray(specificEntitiesToDump))
                specificEntitiesToDump = this.opt.inMemoryEntitiesToDump;
            if (__.IsEmptyString(fileName))
                fileName = this.opt.fileName;
            this.PerformDump(fileName, specificEntitiesToDump, false, onEnd);
        }
        LoadDump(dump, entitiesToLoad, behaviour = LOAD_DUMP_BEHAVIOUR.INSERTBULK, onEnd = null) {
            let me = this;
            if (__.IsNull(dump))
                dump = this.opt.fileName;
            if (!__.IsArray(entitiesToLoad))
                entitiesToLoad = this.opt.inMemoryEntitiesToDump;
            let handleData = function (data) {
                let restoreNextRepo = function (which, previousError) {
                    if (which == entitiesToLoad.length) {
                        //collection finished!
                        if (__.IsFunction(onEnd))
                            onEnd(previousError);
                        return;
                    }
                    let entityRepo = Repository.GetInstance(entitiesToLoad[which], me);
                    entityRepo.LoadDump(data, behaviour, (err) => {
                        //go on on errors...
                        restoreNextRepo((which + 1), previousError || err);
                    });
                };
                restoreNextRepo(0, null);
            };
            //need to read data from a file? If so let's do it ONLY ONCE, don't delegate each repository!
            if (__.IsNotEmptyString(dump)) {
                fs.readFile(dump, "utf-8", (err, data) => {
                    if (__.IsNotNull(err)) {
                        if (__.IsFunction(onEnd))
                            onEnd(err);
                        return;
                    }
                    handleData(JSON.parse(data));
                });
            }
            else
                handleData(dump);
        }
        /**
         * Eventually CONVERTS the data to SQL-Style based on its TYPE
         */
        static FormatFieldValueToSql(data, type) {
            if (type == DB_FIELD_TYPE.DATE) {
                if (__.IsDate(data))
                    return data.getTime().toString();
                else
                    return "NULL";
            }
            else if (type == DB_FIELD_TYPE.NUMBER_INTEGER || type == DB_FIELD_TYPE.NUMBER_DOUBLE) {
                if (__.IsNumber(data))
                    return data.toString();
                else
                    return "NULL";
            }
            else if (type == DB_FIELD_TYPE.TEXT) {
                if (__.IsString(data))
                    return '"' + data.replace(/"/g, "\"\"") + '"';
                else
                    return "NULL";
            }
            return "NULL";
        }
        /**
         * check if the file is already present, in this case load it and MERGE the content replacing the entities in specified dataToSave
         *
         */
        static SaveOrMergeDumpDataToFile(dataToSave, fileName, onEnd) {
            let writeFile = function (data) {
                //Save the dump file
                fs.writeFile(fileName, JSON.stringify(data), onEnd);
            };
            if (fs.existsSync(fileName)) {
                fs.readFile(fileName, "utf-8", (err, foundData) => {
                    if (__.IsNotNull(err)) {
                        if (__.IsFunction(onEnd))
                            onEnd(err);
                        return; //STOP on error
                    }
                    let foundDataObj = JSON.parse(foundData);
                    //REPLACE entities to dump keeping found data
                    dataToSave = __.MergeObj(foundDataObj, dataToSave);
                    writeFile(dataToSave);
                });
            }
            else
                writeFile(dataToSave);
        }
        static DefaultOptions = {
            fileName: "database.sqlite3",
            verbose: false,
            loggingFunction: (msg) => { __.Log(msg); },
            inMemoryMode: true,
            inMemoryDumpEveryMs: 2000,
            inMemoryDumpOnlyIfDirty: true,
            inMemoryEntitiesToDump: []
        };
        Close(callback) {
            let me = this;
            if (me.isClosed)
                return false;
            this.db.close((err) => {
                me.isClosed = true;
                if (__.IsNotNull(me.automaticDumpTimer))
                    clearInterval(me.automaticDumpTimer);
                if (__.IsFunction(callback))
                    callback(err);
            });
            return true;
        }
        /*##########################################################################
         #########################   PROTECTED STUFF   #########################
         ##########################################################################*/
        PerformDump(fileToSave, entitiesToDump, dumpOnlyIfDirty, onEnd) {
            let me = this;
            if (me.opt.verbose)
                me.opt.loggingFunction("[DB " + me.opt.fileName + "] Running automatic dump");
            let dataToSave = {};
            let reposToSetNotDirty = [];
            let dumpNextRepo = function (which) {
                if (which == entitiesToDump.length) {
                    //collection of repos finished!
                    //check if there's something to save
                    let toSaveCount = 0;
                    for (let k in dataToSave) {
                        if (!dataToSave.hasOwnProperty(k))
                            continue;
                        toSaveCount++;
                    }
                    if (toSaveCount == 0) {
                        if (me.opt.verbose)
                            me.opt.loggingFunction("[DB " + me.opt.fileName + "] Nothing to save because nothing is dirty. Automatic dump completed!");
                        me.lastSuccessfulDump = new Date();
                        me.automaticDumpIsProcessing = false;
                        if (__.IsFunction(onEnd))
                            onEnd(null);
                        return;
                    }
                    //SAVE DATA!
                    DB.SaveOrMergeDumpDataToFile(dataToSave, fileToSave, function (writeErr) {
                        if (__.IsNull(writeErr)) {
                            if (me.opt.verbose)
                                me.opt.loggingFunction("[DB " + me.opt.fileName + "] Automatic dump completed!");
                            me.lastSuccessfulDump = new Date();
                            me.automaticDumpIsProcessing = false;
                            reposToSetNotDirty.forEach((repoToSetNotDirty) => {
                                repoToSetNotDirty.SetIsDirty(false);
                            });
                        }
                        else if (me.opt.verbose)
                            me.opt.loggingFunction("[DB " + me.opt.fileName + "] ERROR loading found dump before save: " + JSON.stringify(writeErr));
                        if (__.IsFunction(onEnd)) {
                            onEnd(writeErr);
                        }
                    });
                    return;
                }
                let entityRepo = Repository.GetInstance(entitiesToDump[which], me);
                let entityName = Repository.GetEntityName(entitiesToDump[which]);
                //check if must skip this repo because it's not dirty
                if (dumpOnlyIfDirty == true && entityRepo.GetIsDirty() == false) {
                    if (me.opt.verbose)
                        me.opt.loggingFunction("[DB " + me.opt.fileName + "] Skipping dump for not dirty repository \"" + entityName + "\".");
                    dumpNextRepo(which + 1);
                    return;
                }
                reposToSetNotDirty.push(entityRepo);
                entityRepo.GetAll((data, err) => {
                    if (__.IsNotNull(err)) {
                        if (me.opt.verbose)
                            me.opt.loggingFunction("[DB " + me.opt.fileName + "] ERROR during automatic dump!");
                        me.automaticDumpIsProcessing = false;
                        if (__.IsFunction(onEnd))
                            onEnd(err);
                        return; //STOP on first error
                    }
                    dataToSave[entityName] = data; //keep data to be saved
                    dumpNextRepo(which + 1);
                });
            };
            dumpNextRepo(0);
        }
    }
    IronLibsSqlDb.DB = DB;
    /*#################################################################################################################*/
    /*#################################################################################################################*/
    class Repository {
        Tconstructor;
        db;
        SqlTableName;
        SqlTableFields;
        instanceId;
        isDirty = false;
        static reposInstancesCache = {};
        static GetInstance(Tconstructor, db) {
            let cacheKey = Repository.GetCacheKey(Tconstructor, db);
            if (__.IsNull(Repository.reposInstancesCache[cacheKey]))
                Repository.reposInstancesCache[cacheKey] = new Repository(Tconstructor, db);
            return (Repository.reposInstancesCache[cacheKey]);
        }
        static GetCacheKey(Tconstructor, db) {
            return Repository.GetEntityName(Tconstructor) + "_" + db.GetInstanceId();
        }
        /**
         * DON'T USE directly THIS!
         *
         * @param Tconstructor
         * @param db
         */
        constructor(Tconstructor, db) {
            this.Tconstructor = Tconstructor;
            this.db = db;
            let me = this;
            let cacheKey = Repository.GetCacheKey(Tconstructor, db);
            if (__.IsNotNull(Repository.reposInstancesCache[cacheKey]))
                throw new Error("Error: Repository instantiation failed: use Repository.GetInstance() instead of new.");
            else
                Repository.reposInstancesCache[cacheKey] = this;
            me.instanceId = __.Guid.New();
            let t = new Tconstructor();
            me.SqlTableName = t.SqlTableName();
            me.SqlTableFields = t.SqlTableFields();
        }
        static GetEntityName(typeConstructor) {
            let txt = typeConstructor.toString();
            let startPos = txt.indexOf(" ") + 1;
            return txt.substr(startPos, txt.indexOf("(") - startPos);
        }
        GetIsDirty() {
            return this.isDirty;
        }
        SetIsDirty(dirtyness) {
            this.isDirty = dirtyness;
        }
        /**
         * Ensures that the data table for the repository type is present
         * @param forceRecreate If TRUE all found table data will be erased
         * @param onEnd
         */
        Init(forceRecreate = false, onEnd) {
            let me = this;
            if (forceRecreate == true)
                me.RecreateTable(onEnd);
            else
                me.db.ExecuteSql(me.GetSqlCreateTable(true), onEnd);
        }
        /**
         * Destroys if exists and recreates the empty table.
         * Useful to DELETE ALL ITS DATA.
         */
        RecreateTable(onEnd) {
            let me = this;
            this.db.Transaction(function () {
                me.db.ExecuteSql("DROP TABLE IF EXISTS " + me.SqlTableName, (err) => {
                    if (__.IsNotNull(err)) {
                        if (__.IsFunction(onEnd))
                            onEnd(err);
                    }
                    else {
                        me.isDirty = true;
                        me.db.ExecuteSql(me.GetSqlCreateTable(false), onEnd);
                    }
                });
            });
        }
        LoadDump(dump, behaviour = LOAD_DUMP_BEHAVIOUR.INSERTBULK, onEnd = null) {
            let me = this;
            let myTypeName = Repository.GetEntityName(me.Tconstructor);
            if (__.IsNull(dump))
                dump = me.db.GetFileName();
            if (me.db.GetIsVerbose())
                me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] Loading dump for type \"" + myTypeName + "\"");
            let handleData = function (data) {
                //ok, here we have the dump data, try to cast to my type
                let castedData = data[myTypeName].map((record) => {
                    return me.ConvertObjectToType(record);
                });
                let insertData = function () {
                    if (behaviour == LOAD_DUMP_BEHAVIOUR.UPDATEIFFOUND)
                        me.SaveAll(castedData, (result, err) => {
                            if (me.db.GetIsVerbose()) {
                                if (__.IsNull(err))
                                    me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] Dump loaded checking to update!");
                                else
                                    me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] ERROR loading dump: " + JSON.stringify(err));
                            }
                            if (__.IsFunction(onEnd))
                                onEnd(err);
                        });
                    else
                        me.InsertBulk(castedData, (err) => {
                            if (me.db.GetIsVerbose()) {
                                if (__.IsNull(err))
                                    me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] Dump loaded with InsertBulk!");
                                else
                                    me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] ERROR loading dump: " + JSON.stringify(err));
                            }
                            if (__.IsFunction(onEnd))
                                onEnd(err);
                        });
                };
                //now check if need to delete previous data
                if (behaviour == LOAD_DUMP_BEHAVIOUR.DELETEALLDATA) {
                    me.RecreateTable((err) => {
                        if (__.IsNotNull(err)) {
                            if (me.db.GetIsVerbose())
                                me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] ERROR loading dump: " + JSON.stringify(err));
                            if (__.IsFunction(onEnd))
                                onEnd(err);
                            return; //STOP on error
                        }
                        insertData();
                    });
                }
                else
                    insertData();
            };
            //need to read data from a file?
            if (__.IsNotEmptyString(dump)) {
                fs.readFile(dump, "utf-8", (err, data) => {
                    if (__.IsNotNull(err)) {
                        if (me.db.GetIsVerbose())
                            me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] ERROR loading dump: " + JSON.stringify(err));
                        if (__.IsFunction(onEnd))
                            onEnd(err);
                        return; //STOP on error
                    }
                    handleData(JSON.parse(data));
                });
            }
            else
                handleData(dump);
        }
        /**
         * Checks if an Entity exists in this repository (search by PrimaryKey).
         *
         * @param pk  an Entity PrimaryKey
         * @param onEnd Called with "result" containing the check, or with non-null "err" in case of error
         */
        Exists(pk, onEnd) {
            let me = this;
            if (!__.IsFunction(onEnd))
                onEnd = () => { };
            let pkInfo = me.SqlTableFields.filter((x) => x.primary)[0];
            if (__.IsNull(pkInfo)) {
                setTimeout(function () {
                    onEnd(null, new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }
            let sql = "select count(*) from " + me.SqlTableName + " where [" + pkInfo.name + "] = " + DB.FormatFieldValueToSql(pk, pkInfo.type) + ";";
            me.db.ExecuteSelect(sql, function (data, err) {
                let result = false;
                if (__.IsNull(err)) {
                    result = data[0]["count(*)"] == 1;
                }
                if (__.IsFunction(onEnd))
                    onEnd(result, err);
            });
        }
        /**
         * Deletes the passed entity (search by PrimaryKey) IF IT EXISTS in this repository.
         *
         * @param pk  an Entity PrimaryKey
         * @param onEnd Called with non-null "err" in case of error
         */
        Delete(pk, onEnd) {
            let me = this;
            if (!__.IsFunction(onEnd))
                onEnd = () => { };
            let pkInfo = me.SqlTableFields.filter((x) => x.primary)[0];
            if (__.IsNull(pkInfo)) {
                setTimeout(function () {
                    onEnd(new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }
            let sql = "DELETE from " + me.SqlTableName + " where [" + pkInfo.name + "] = " + DB.FormatFieldValueToSql(pk, pkInfo.type) + ";";
            me.db.ExecuteSql(sql, (err) => {
                if (__.IsNull(err))
                    me.SetIsDirty(true);
                onEnd(err);
            });
        }
        GetCount(onEnd, whereClause) {
            let me = this;
            let sql = "SELECT count(*) from " + me.SqlTableName;
            if (__.IsNotEmptyString(whereClause))
                sql += " WHERE " + whereClause;
            me.db.ExecuteSelect(sql, function (countData, err) {
                let count = -1;
                if (__.IsNull(err))
                    count = countData[0]["count(*)"];
                if (__.IsFunction(onEnd))
                    onEnd(count, err);
            });
        }
        /**
         * Returns ALL entities in this repository.
         *
         * @param onEnd Called with "data" containing the entities, or with non-null "err" in case of error
         * @param collectionsToFill Specify which collections (properties of the retrieved entities) to retrieve
         */
        GetAll(onEnd, collectionsToFill = null) {
            this.GetWhere("", onEnd, collectionsToFill);
        }
        /**
         * Returns ALL entities in this repository satisfying the passed clause
         *
         * @param whereClause The SQL where clause
         * @param onEnd Called with "data" containing the entities, or with non-null "err" in case of error
         * @param collectionsToFill Specify which collections (properties of the retrieved entities) to retrieve
         * @param onlySpecifiedFields
         */
        GetWhere(whereClause, onEnd, collectionsToFill = null, onlySpecifiedFields = null) {
            let me = this;
            if (__.IsNull(collectionsToFill))
                collectionsToFill = [];
            if (!__.IsFunction(onEnd))
                onEnd = () => { };
            let pkInfo = me.SqlTableFields.filter((x) => x.primary)[0];
            if (__.IsNull(pkInfo)) {
                setTimeout(function () {
                    onEnd(null, new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }
            let sql = "SELECT ";
            if (__.IsNotEmptyArray(onlySpecifiedFields))
                sql += onlySpecifiedFields.map((x) => "[" + x + "]").join(",");
            else
                sql += "*";
            sql += " from " + me.SqlTableName;
            if (__.IsNotEmptyString(whereClause))
                sql += " WHERE " + whereClause;
            me.db.ExecuteSelect(sql, function (data, err) {
                let mainResult = null;
                if (__.IsNull(err)) {
                    mainResult = data.map(function (dbRow) {
                        return me.ConvertObjectToType(dbRow);
                    });
                }
                let asyncFunctions = [];
                //now check for COLLECTIONS to be retrieved
                if (__.IsArray(mainResult)) {
                    mainResult.forEach(function (record) {
                        collectionsToFill.forEach(function (collectionFieldName) {
                            let collection = (record[collectionFieldName]);
                            let repo = Repository.GetInstance(collection.Tconstructor(), me.db);
                            asyncFunctions.push(function (notifyEnd) {
                                //Get the elements of the collection
                                //TODO: here it's possible to select some nested collections...
                                repo.GetWhere(collection.FkName() + " = " + DB.FormatFieldValueToSql(record[pkInfo.name], pkInfo.type), function (data, err) {
                                    if (__.IsNull(err)) {
                                        //replace the collection with the retrieved values
                                        collection.LoadWithValues(data);
                                    }
                                    notifyEnd(err); //return the error if present
                                });
                            });
                        });
                    });
                }
                //the JOINING function
                asyncFunctions.push(function (resultsOrderedByPosition, resultsOrderedByTime) {
                    let errToReturn = Repository.GetFirstNotNullError(err, resultsOrderedByTime);
                    onEnd(mainResult, errToReturn); //pass the FIRST error received
                });
                __.ExecuteAndJoin(asyncFunctions);
            });
        }
        /**
         * Returns the entity with the specified PrimaryKey in this repository.
         *
         * @param pk
         * @param onEnd Called with "data" containing the entity if found or NULL if not found,
         * or with non-null "err" in case of error
         * @param collectionsToFill
         */
        GetByPk(pk, onEnd, collectionsToFill = null) {
            let me = this;
            if (!__.IsFunction(onEnd))
                onEnd = () => { };
            let pkInfo = me.SqlTableFields.filter((x) => x.primary)[0];
            if (__.IsNull(pkInfo)) {
                setTimeout(function () {
                    onEnd(null, new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }
            this.GetWhere("[" + pkInfo.name + "] = " + DB.FormatFieldValueToSql(pk, pkInfo.type), function (data, err) {
                let result = null;
                if (__.IsNull(err) && data.length > 0) {
                    result = data[0];
                }
                if (__.IsFunction(onEnd))
                    onEnd(result, err);
            }, collectionsToFill);
        }
        /**
         * Returns the entities with the specified PrimaryKeys in this repository.
         *
         * @param pks
         * @param onEnd Called with "data" containing the entities found, or with non-null "err" in case of error
         * @param collectionsToFill Specifies which collections (properties of the retrieved entities) to retrieve
         */
        GetByPks(pks, onEnd, collectionsToFill = null) {
            let me = this;
            if (!__.IsFunction(onEnd))
                onEnd = () => { };
            let pkInfo = me.SqlTableFields.filter((x) => x.primary)[0];
            if (__.IsNull(pkInfo)) {
                setTimeout(function () {
                    onEnd(null, new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }
            let idsList = pks.map((pk) => { return DB.FormatFieldValueToSql(pk, pkInfo.type); });
            this.GetWhere("[" + pkInfo.name + "] in (" + idsList.join(",") + ")", onEnd, collectionsToFill);
        }
        /**
         * Executes an OPTIIMIZED bulk INSERT for all passed entities which MUST BE NEW.
         * WARNING: if any of them already exist then an error will be returned and NO entity will be inserted!
         *
         * @param items The NEW entities
         * @param onEnd Called with non-null "err" in case of error
         */
        InsertBulk(items, onEnd) {
            let me = this;
            let sql = "INSERT INTO " + me.SqlTableName + "(";
            me.SqlTableFields.forEach(function (field, i) {
                sql += "[" + field.name + "]";
                if (i < me.SqlTableFields.length - 1)
                    sql += ",";
            });
            sql += ") VALUES (";
            items.forEach(function (data, dataIdx) {
                sql += me.GetSqlEntityValues(data) + ")";
                if (dataIdx < items.length - 1)
                    sql += ",(";
            });
            me.db.ExecuteSql(sql, (err) => {
                if (__.IsNull(err))
                    me.SetIsDirty(true);
                if (__.IsFunction(onEnd))
                    onEnd(err);
            });
        }
        /**
         * Persists the passed entities.
         * Expects a Not-Null items, otherwise the callback will be called with a specific error.
         * A CHECK is made whether each item needs to be INSERTED or UPDATED.
         * WARNING: not so performing... since (2*items.length) queries are performed.
         *
         * @param items The entities to be persisted
         * @param onEnd  Called with "created" == true if INSERT, == false if UPDATE for each item, or with non-null "err" in case of error
         * @param collectionsToSave Specifies which collections (properties of the saved entities) to save
         */
        SaveAll(items, onEnd, collectionsToSave = null) {
            let me = this;
            let asyncFunctions = [];
            let resultsCreated = [];
            items.forEach(function (item, itemIdx) {
                asyncFunctions.push(function (notifyEnd) {
                    //Save ALL the elements in ANY order
                    me.Save(item, function (created, err) {
                        resultsCreated[itemIdx] = created; //store the "created" information at the right position
                        notifyEnd(err); //return the error if present
                    }, collectionsToSave);
                });
            });
            //the JOINING function
            asyncFunctions.push(function (resultsOrderedByPosition, resultsOrderedByTime) {
                if (__.IsFunction(onEnd)) {
                    let errToReturn = Repository.GetFirstNotNullError(null, resultsOrderedByTime);
                    onEnd(resultsCreated, errToReturn); //pass the FIRST error received and the "created" array
                }
            });
            __.ExecuteAndJoin(asyncFunctions);
        }
        /**
         * Persists the passed entity.
         * Expects a Not-Null obj, otherwise the callback will be called with a specific error.
         * A CHECK is made whether it needs to be INSERTED or UPDATED.
         * WARNING: not so performing... since 2 queries are performed.
         *
         * @param obj The entity to be persisted
         * @param onEnd  Called with "created" == true if INSERT, == false if UPDATE, or with non-null "err" in case of error
         * @param collectionsToSave Specifies which collections (properties of the saved entity) to save
         */
        Save(obj, onEnd, collectionsToSave = null) {
            let me = this;
            if (__.IsNull(collectionsToSave))
                collectionsToSave = [];
            if (!__.IsFunction(onEnd))
                onEnd = () => { };
            if (__.IsNull(obj)) {
                setTimeout(function () {
                    onEnd(false, new Error("NULL obj passed to Repository.Save"));
                }, 1);
                return;
            }
            let pkInfo = me.SqlTableFields.filter((x) => x.primary)[0];
            if (__.IsNull(pkInfo)) {
                setTimeout(function () {
                    onEnd(false, new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }
            //must CHECK if needs to be INSERTED or UPDATED
            me.Exists(obj[pkInfo.name], function (exists, err) {
                if (__.IsNotNull(err)) {
                    onEnd(false, err);
                    return;
                }
                /**
                 * Internal function which fires async save of all collectionsToSave
                 * @param finalCreated Specifies what to pass to onEnd
                 */
                let SaveAllCollections = function (finalCreated) {
                    let asyncFunctions = [];
                    collectionsToSave.forEach(function (collectionFieldName) {
                        let collection = (obj[collectionFieldName]);
                        let repo = Repository.GetInstance(collection.Tconstructor(), me.db);
                        let subitemsToSave = obj[collectionFieldName].Values();
                        subitemsToSave.forEach((el) => { el[collection.FkName()] = obj[pkInfo.name]; }); //link all items in the collection to their parent
                        asyncFunctions.push(function (notifyEnd) {
                            repo.SaveAll(subitemsToSave, function (created, err2) {
                                notifyEnd(err2);
                            });
                        });
                    });
                    //the JOINING function
                    asyncFunctions.push(function (resultsOrderedByPosition, resultsOrderedByTime) {
                        let errToReturn = Repository.GetFirstNotNullError(null, resultsOrderedByTime);
                        onEnd(finalCreated, errToReturn); //pass the FIRST error received and the "created" array
                    });
                    __.ExecuteAndJoin(asyncFunctions);
                };
                if (exists) {
                    //create the sql statement
                    let sql = "UPDATE " + me.SqlTableName + " set ";
                    me.SqlTableFields.forEach(function (field, i) {
                        sql += "[" + field.name + "] = " + DB.FormatFieldValueToSql(obj[field.name], field.type);
                        if (i < me.SqlTableFields.length - 1)
                            sql += ",";
                    });
                    sql += " WHERE [" + pkInfo.name + "] = " + DB.FormatFieldValueToSql(obj[pkInfo.name], pkInfo.type) + ";";
                    me.db.ExecuteSql(sql, function (err) {
                        if (__.IsNotNull(err)) {
                            onEnd(false, err); // "created" == false, eventual error passed straight away
                            return;
                        }
                        me.SetIsDirty(true);
                        if (collectionsToSave.length == 0) {
                            onEnd(false, err); // "created" == false, eventual error passed straight away
                        }
                        else
                            SaveAllCollections(false); //save was OK, go on with specified collections
                    });
                }
                else {
                    me.InsertBulk([obj], function (err) {
                        if (__.IsNotNull(err) || collectionsToSave.length == 0) {
                            if (__.IsFunction(onEnd))
                                onEnd(true, err); // "created" == false, eventual error passed straight away
                        }
                        else
                            SaveAllCollections(true); //save was OK, go on with specified collections
                    });
                }
            });
        }
        /*##########################################################################
         #########################   PROTECTED STUFF   #########################
         ##########################################################################*/
        /**
         * Returns the first NON null error from the one passed and eventual errors returned by the async functions whose results have been passed
         */
        static GetFirstNotNullError(err, results) {
            if (__.IsNotNull(err))
                return err;
            if (__.IsArray(results)) {
                for (let i = 0; i < results.length; i++) {
                    if (__.IsNotNull(results[i].SuccessfulResult)) //this query returned an error
                        return results[i].SuccessfulResult;
                    if (__.IsNotNull(results[i].Exception)) //this query threw an exception
                        return results[i].Exception;
                }
            }
            return null;
        }
        /**
         * Tries to convert a PLAIN JavaScript object to a T-Typed instance
         */
        ConvertObjectToType(obj) {
            let me = this;
            //CONVERT to the specified type
            let objT = new me.Tconstructor();
            me.SqlTableFields.forEach(function (field) {
                //Eventually CONVERT the data to be written
                if (field.type == DB_FIELD_TYPE.DATE && __.IsNotNull(obj[field.name]))
                    objT[field.name] = new Date(obj[field.name]);
                else
                    objT[field.name] = obj[field.name];
            });
            return objT;
        }
        /**
         * Returns the fields SQL-VALUES of the given record
         */
        GetSqlEntityValues(data) {
            let me = this;
            let sql = "";
            me.SqlTableFields.forEach(function (field, i) {
                sql += DB.FormatFieldValueToSql(data[field.name], field.type);
                if (i < me.SqlTableFields.length - 1)
                    sql += ",";
            });
            return sql;
        }
        GetSqlCreateTable(ifNotExist) {
            let ret = "CREATE TABLE" +
                (ifNotExist ? " IF NOT EXISTS " : " ") +
                this.SqlTableName +
                "(";
            let fields = this.SqlTableFields;
            let primaryKeyName = null;
            fields.forEach(function (f, i) {
                ret += "'" + f.name + "' ";
                if (f.type == DB_FIELD_TYPE.TEXT)
                    ret += "TEXT";
                else if (f.type == DB_FIELD_TYPE.NUMBER_INTEGER)
                    ret += "INTEGER";
                else if (f.type == DB_FIELD_TYPE.NUMBER_DOUBLE)
                    ret += "DOUBLE";
                else if (f.type == DB_FIELD_TYPE.DATE)
                    ret += "INTEGER";
                else if (f.type == DB_FIELD_TYPE.BINARY)
                    ret += "BINARY";
                if (f.primary == true) {
                    ret += " PRIMARY KEY UNIQUE NOT NULL";
                    primaryKeyName = f.name;
                }
                if (i < fields.length - 1)
                    ret += ", ";
            });
            ret += ");";
            if (__.IsNotEmptyString(primaryKeyName))
                ret += " CREATE INDEX " + this.SqlTableName + "_PK on " + this.SqlTableName + "(" + primaryKeyName + ");";
            return ret;
        }
    }
    IronLibsSqlDb.Repository = Repository;
})(IronLibsSqlDb = exports.IronLibsSqlDb || (exports.IronLibsSqlDb = {}));
//# sourceMappingURL=sqlDb.js.map