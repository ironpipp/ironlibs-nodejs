import puppeteer = require('puppeteer-core');
export declare module IronLibsPuppeteer {
    function GetInnerText(page: puppeteer.Page, el: puppeteer.ElementHandle): Promise<string>;
    function GetInnerHtml(page: puppeteer.Page, el: puppeteer.ElementHandle): Promise<string>;
}
