"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IronLibsExpressMVC = void 0;
const express = require("express");
var IronLibsExpressMVC;
(function (IronLibsExpressMVC) {
    let __ = require("ironlibs-base/build/common").IronLibsCommon; //using ": typeof IronLibsCommon" we get the same as "import {IronLibsCommon as __} from './common';"
    let NotificationType;
    (function (NotificationType) {
        NotificationType[NotificationType["ERROR"] = 0] = "ERROR";
        NotificationType[NotificationType["SUCCESS"] = 1] = "SUCCESS";
        NotificationType[NotificationType["INFO"] = 2] = "INFO";
    })(NotificationType = IronLibsExpressMVC.NotificationType || (IronLibsExpressMVC.NotificationType = {}));
    class ControllerBase {
        constructor() {
            this.router = express.Router();
        }
        router = null;
        GetRouter() { return this.router; }
        /**
         * Registers a controller action invoked for ANY http verb
         *
         * @param url one or more urls (also with wildcards) to be matched in order to call the callback
         * @param clbk The action executed upon an url match
         */
        RegisterActionAll(url, clbk) {
            let me = this;
            let urls = __.IsArray(url) ? url : [url];
            urls.forEach(function (u) {
                me.router.all(u, clbk);
            });
        }
        /**
         * Registers a controller action invoked for GET http verb
         *
         * @param url one or more urls (also with wildcards) to be matched in order to call the callback
         * @param clbk The action executed upon an url match
         */
        RegisterActionGet(url, clbk) {
            let me = this;
            let urls = __.IsArray(url) ? url : [url];
            urls.forEach(function (u) {
                me.router.get(u, clbk);
            });
        }
        /**
         * Registers a controller action invoked for POST http verb
         *
         * @param url one or more urls (also with wildcards) to be matched in order to call the callback
         * @param clbk The action executed upon an url match
         */
        RegisterActionPost(url, clbk) {
            let me = this;
            let urls = __.IsArray(url) ? url : [url];
            urls.forEach(function (u) {
                me.router.post(u, clbk);
            });
        }
        Respond(res, msg, statusCode = 200, headerValues = null) {
            res.statusCode = statusCode;
            if (__.IsNotNullObject(headerValues)) {
                for (let key in headerValues) {
                    if (!headerValues.hasOwnProperty(key))
                        continue;
                    res.header(key, headerValues[key]);
                }
            }
            res.status(statusCode);
            if (__.IsNull(msg))
                msg = { IsValid: (statusCode >= 200 && statusCode < 300), Notifications: [] };
            if (__.IsString(msg))
                res.write(msg);
            else
                res.json(msg);
            res.end();
        }
        RespondOk(res, data = null, notification = null, type = NotificationType.SUCCESS) {
            let ret = {
                IsValid: true,
                Notifications: [],
            };
            if (__.IsNotNull(data))
                ret.Data = data;
            if (__.IsString(notification))
                ret.Notifications.push({ MessageText: notification, Type: type });
            this.Respond(res, ret, 200);
        }
        RespondError(res, data = null, notification = null, clientError = true) {
            let ret = {
                IsValid: false,
                Notifications: [],
            };
            if (__.IsNotNull(data))
                ret.Data = data;
            if (__.IsString(notification))
                ret.Notifications.push({ MessageText: notification, Type: NotificationType.ERROR });
            this.Respond(res, ret, clientError ? 400 : 500);
        }
        RespondRedirect(res, where) {
            this.Respond(res, "", 302, { location: where });
        }
    }
    IronLibsExpressMVC.ControllerBase = ControllerBase;
})(IronLibsExpressMVC = exports.IronLibsExpressMVC || (exports.IronLibsExpressMVC = {}));
//# sourceMappingURL=expressMVC.js.map