/// <reference types="node" />
import https = require("https");
export declare module IronLibsNodeJs {
    function IsUnixEnvironment(): boolean;
    function GetDirectoriesSeparator(): string;
    /**
     * Returns the APPLICATION DATA directory with the ending separator (ex: "/")
     */
    function GetDataDirectory(appName: string, ensureExists?: boolean): string;
    /**
     * Reads package.json and ensures all required modules are installed. Will exit if one or more is not found.
     */
    function CheckModules(): void;
    /**
     * Converts an ArrayBuffer to an UTF8 string
     */
    function ArrayBufferToUtf8String(buff: ArrayBuffer): string;
    /**
     * Return the BASE64 string of the SHA1 hash of the passed string
     */
    function GetHash(toBeHashed: string): string;
    /**
     * Accepts only a FILE name (with extension, NO path segments).
     * Returns a valid one.
     * Requires "sanitize-filename" package
     */
    function SanitizeFileName(fileName: string, replaceInvalidCharsWith?: string): string;
    /**
     * The SANITIZED paths tree will be returned (without file!). Il will always end with pathSegmentsSeparator.
     * Requires "sanitize-filename" package
     */
    function SanitizeFolderPath(fullPath: string, replaceInvalidCharsWith?: string, pathSegmentsSeparator?: string): string;
    /**
     * Ensures that all subdirectories exist. If one or more are missing creates them.
     *
     * fullPath MUST NOT point to a file, ONLY DIRECTORIES!
     * fullPath CAN contain invalid characters. But only its SANITIZED version is created and returned.
     */
    function EnsureSanitizedPathsTreeIsCreated(fullPath: string, replaceInvalidCharsWith?: string): string;
    /**
     * Checks if the passed string is an absolute path (valid for both UNIX and Windows)
     * Examples of absolute paths: \    \asd\qwe    /asd/qwe    d:\  c:\asd\qwe
     * @param s
     */
    function IsAbsolutePath(s: string): boolean;
    function CopyFile(sourceFile: string, destFile: string, onEnd?: (err?: Error) => void, deleteSourceAfterSuccessfulCopy?: boolean): void;
    /**
     * Converts an ArrayBuffer to an UTF8 string
     */
    function ArrayBufferToBlob(buff: ArrayBuffer): string;
    function OpenWithExplorer(path: string): void;
    /**
     *  Compresses passed data to a valid .7z archive (contains only ONE file named "compressed")
     *  Requires "lzma" package
     *
     * @param dataToCompress The data to compress in any form
     * @param compressionLevel from 1 (fastest) to 9 (best)
     * @param onEnd The "compressedData" param can be converted to a typed array calling "new Uint8Array(compressedData)". The "error" param is 0 upon success or the Error object otherwise
     * @param onProgress The "progress" param goes from 0 to 1
     */
    function LzmaCompress(dataToCompress: string | number[] | Buffer | Uint8Array, compressionLevel?: number, onEnd?: (compressedData: number[], error: Error | number) => void, onProgress?: (progress: number) => void): void;
    /**
     *  Decompresses back the passed compressed data
     *  Requires "lzma" package
     *
     * @param compressedData The binary data to decompress
     * @param onEnd The "decompressedData" param is a string if it decodes as valid UTF-8 text, otherwise will be a Uint8Array instance. The "error" param is 0 upon success or the Error object otherwise
     * @param onProgress The "progress" param goes from 0 to 1
     */
    function LzmaDecompress(compressedData: number[] | Buffer | Uint8Array, onEnd?: (decompressedData: string | Uint8Array, error: {
        message: string;
    } | number) => void, onProgress?: (progress: number) => void): void;
    interface ISimpleWebServerOptions {
        port?: number;
        /**
         * The BASE URL PATH the web server will respond to (defaults to "/")
         */
        basePath?: string;
        /**
         * An array of mapping to choose the filesystem directory to use when requesting a web resource.
         * Will be used the FIRST rule matching "pathStartingWith", so put the most generic at the end of the array.
         * "serveFromDirectory" must be an ABSOLUTE path.
         * Example:
         *
         * [{
         *     pathStartingWith   : "/lib/",
         *     serveFromDirectory : path.resolve(__dirname, "..", "node_modules")
         * },
         * {
         *     pathStartingWith   : "/",
         *     serveFromDirectory : path.resolve(__dirname, "..", "static")
         * }]
         */
        staticPathsMapping?: {
            pathStartingWith: string;
            serveFromDirectory: string;
        }[];
        onListening?: (error: boolean) => void;
        /**
         * Sets the "cache-control" response header for responses handled by staticPaths
         */
        cacheControlForStaticPaths?: string;
        /**
         * Callback fired upon new web request received.
         * If this callback returns TRUE then the default responding flow will terminate
         * (so meaning that this callback execution has handled the request and its response),
         * otherwise the responding flow will execute normally returning the static content.
         *
         * @param res
         * @param req
         * @returns {boolean}
         */
        onWebRequest?: (res: uWS.HttpResponse, req: uWS.HttpRequest) => boolean;
        useSSL?: boolean;
        sslKeyFileName?: string;
        sslCertFileName?: string;
    }
    /**
     * Simply starts a web server serving static resources mapped to one or more directories on file system.
     * Can be used also for dynamic content (ex a small API) using onWebRequest callback
     *
     * @requires  {} NPM Libraries 'uWebSockets.js' and 'mime-types'
     * @param opt
     */
    function StartSimpleWebServer(opt: ISimpleWebServerOptions): void;
    function StopAllSimpleWebServers(): void;
    /**
     * Helper function for reading posted data
     * @param res
     * @param onReadCompleted Called when all posted data has been read and parsed
     * @param onError Called when an error occurs
     */
    function ReadRequestBody(res: uWS.HttpResponse, onReadCompleted: (json: any, str: string) => void, onError: () => void): void;
    /**
     * Helper function for answering a request with json data
     * @param res
     * @param status the HTTP status to respond
     * @param content an eventual JSON data to respond
     */
    function Respond(res: uWS.HttpResponse, status: number, content?: any): void;
    /**
     * Makes a Server to Server HTTP[S] call and notifies the caller SURELY 1 ONLY TIME with
     * an instance of Error, or otherwise with the body of the response (JSON / text / binary)
     *
     * @param requestOptions
     * @param onCallEnd The callback is called FOR SURE ONLY ONE TIME with
     * an instance of Error, or otherwise with the body of the response (JSON / text) together with its HTTP StatusCode
     */
    function S2SCall(requestOptions: https.RequestOptions, onCallEnd: (result?: any, httpStatusCode?: number) => void): void;
    /**
     * Represent a configuration file manager: permits to get/set its values and eventually read/save a JSON file
     */
    class ConfigurationManager<ConfType extends Object> {
        private currentFileName;
        private currentConfiguration;
        private defaultConfiguration;
        constructor(fileName: string, createIfNotFound?: boolean, defaultConfiguration?: ConfType);
        /**
         * Returns the current "in memory" configuration value
         * WARNING: the returned value is CLONED (so can't change it!)
         */
        GetCurrentConfiguration(): ConfType;
        /**
         * Updates the "in memory" configuration value
         * WARNING: doesn't write files automatically!
         *
         * @param newConfiguration
         */
        SetCurrentConfiguration(newConfiguration: ConfType): void;
        GetCurrentFileName(): string;
        /**
         * Updates the "in memory" configuration value reading it form file
         */
        LoadCurrentConfigurationFromFile(): void;
        /**
         * Saves to the file the current "in memory" configuration value
         */
        SaveCurrentConfigurationToFile(): void;
        /**
         * Sets a "default manager instance" for a particular "topic/argument".
         * Ex. use case:
         * At app start we create a manager instance and call SetManagerForTopic with "MainConf" topic.
         * All other modules needing it call ConfigurationManager.GetManagerForTopic to obtain it.
         */
        static SetManagerForTopic<T>(topic: string, manager: ConfigurationManager<T>): void;
        /**
         * Returns the "default manager instance" for a particular "topic/argument", if ever set
         */
        static GetManagerForTopic<T>(topic: string): ConfigurationManager<T>;
    }
    module ServerResources {
        /**
         * Loads in memory EVERY available LANGUAGE from files.
         */
        function LoadResourcesFiles(rootPath: string, fileTemplate?: string): void;
        /**
         * Sets the current language for the SERVER
         */
        function SetCurrentLanguage(defaultServerLanguageToSet: string): void;
        /**
         * Gets the current language for the SERVER
         */
        function GetCurrentLanguage(): string;
        /**
         * Returns a resources dictionary.
         * Returns the specified language (if supported) or the default one
         */
        function GetResources<T>(forceLanguage?: string): T;
        function IsValidLanguage(lang: string): boolean;
        function GetAvailableLanguages(): string[];
        function SetAvailableLanguages(languages: string[]): void;
    }
}
