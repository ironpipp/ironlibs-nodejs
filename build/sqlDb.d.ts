import { IronLibsCommon } from "ironlibs-base";
export declare module IronLibsSqlDb {
    enum DB_FIELD_TYPE {
        TEXT = 0,
        NUMBER_INTEGER = 1,
        NUMBER_DOUBLE = 2,
        DATE = 3,
        BINARY = 4
    }
    enum LOAD_DUMP_BEHAVIOUR {
        INSERTBULK = 0,
        UPDATEIFFOUND = 1,
        DELETEALLDATA = 2
    }
    interface IDBFieldDescriptor {
        name: string;
        type: DB_FIELD_TYPE;
        primary?: boolean;
    }
    interface IEntity {
        SqlTableName(): string;
        SqlTableFields(): IDBFieldDescriptor[];
        SqlTableFieldNames(): string[];
    }
    class Entity implements IEntity {
        constructor();
        SqlTableName(): string;
        SqlTableFields(): IDBFieldDescriptor[];
        SqlTableFieldNames(): string[];
    }
    class IdEntity extends Entity {
        Id: string;
        SqlTableName(): string;
        SqlTableFields(): IDBFieldDescriptor[];
    }
    class EntityCollection<T extends Entity> {
        private tconstructor;
        private fkName;
        private values;
        private isLoaded;
        constructor(tconstructor: Function, fkName: string, values?: T[], isLoaded?: boolean);
        Values(): T[];
        IsLoaded(): boolean;
        FkName(): string;
        Tconstructor(): Function;
        LoadWithValues(values: T[]): void;
    }
    class DB {
        private db;
        private opt;
        private instanceId;
        private lastSuccessfulDump;
        private automaticDumpTimer;
        private automaticDumpPaused;
        private automaticDumpIsProcessing;
        private isClosed;
        /**
         *
         * @param options Sets the following possible options:
         *        fileName : Specifies the filename of the stored database file, or when inMemoryMode == true specifies the filename of the dump file.
         *        verbose : Specifies whether to log SQL statements or not.
         *        loggingFunction : Specifies the logging function accepting the string to log.
         *        inMemoryMode : If FALSE data is stored immediately (BAD queries performances, especially for non-SSD hard disks).
         *                       If TRUE data is kept in memory and persisted only in intervals of inMemoryDumpEveryMs milliseconds.
         *        inMemoryDumpEveryMs : Has sense only when inMemoryMode == true.
         *                              If > 0  data is persisted automatically as a dump every the specified interval in milliseconds.
         *                              If <= 0 data is NEVER persisted automatically.
         *        inMemoryDumpOnlyIfDirty : If TRUE the automatic dump is updated only if data has changed in any repository.
         *                                  If FALSE the dump is always updated.
         *        inMemoryRepositoriesToDump : The repositories to use to dump data. WARNING: only data of these repositories will be persisted automatically!
         */
        constructor(options?: DB.IOptions);
        GetInstanceId(): string;
        GetIsVerbose(): boolean;
        GetInMemoryEntitiesToDump(): Function[];
        GetLoggingFunction(): (msg: string) => void;
        GetFileName(): string;
        GetLastSuccessfulDump(): Date;
        ExecuteSelect(sql: string, onEnd: (data: any[], err: Error) => void): void;
        ExecuteSql(sql: string, callback?: (err: Error) => void): void;
        Transaction(callback?: () => void, serial?: boolean): void;
        /**
         * Pauses an already started automatic dump
         */
        PauseAutomaticDump(): void;
        /**
         * Pauses an already paused automatic dump
         */
        RestoreAutomaticDump(): void;
        /**
         * Executes a dump independently from the automatic system.
         *
         * @param fileName Specify the file to be saved. If NULL then options.fileName will be used
         * @param specificEntitiesToDump Specify the entities types to dump. If NULL then options.inMemoryEntitiesToDump will be used
         * @param onEnd Called with non-null "err" in case of error
         */
        ForceDump(fileName?: string, specificEntitiesToDump?: Function[], onEnd?: (err: Error) => void): void;
        LoadDump(dump: string, entitiesToLoad?: Function[], behaviour?: LOAD_DUMP_BEHAVIOUR, onEnd?: (err: Error) => void): any;
        /**
         * Eventually CONVERTS the data to SQL-Style based on its TYPE
         */
        static FormatFieldValueToSql(data: any, type: DB_FIELD_TYPE): string;
        /**
         * check if the file is already present, in this case load it and MERGE the content replacing the entities in specified dataToSave
         *
         */
        static SaveOrMergeDumpDataToFile(dataToSave: any, fileName: string, onEnd?: (err: Error) => void): void;
        static DefaultOptions: DB.IOptions;
        Close(callback?: (err: Error | null) => void): boolean;
        protected PerformDump(fileToSave: string, entitiesToDump: Function[], dumpOnlyIfDirty: boolean, onEnd?: (err: Error) => void): void;
    }
    module DB {
        interface IOptions {
            fileName?: string;
            verbose?: boolean;
            loggingFunction?: (msg: string) => void;
            inMemoryMode?: boolean;
            inMemoryDumpEveryMs?: number;
            inMemoryDumpOnlyIfDirty?: boolean;
            inMemoryEntitiesToDump?: Function[];
        }
    }
    class Repository<T extends Entity> {
        private Tconstructor;
        private db;
        private SqlTableName;
        private SqlTableFields;
        private instanceId;
        private isDirty;
        private static reposInstancesCache;
        static GetInstance<T extends Entity>(Tconstructor: Function, db: DB): Repository<T>;
        protected static GetCacheKey(Tconstructor: Function, db: DB): string;
        /**
         * DON'T USE directly THIS!
         *
         * @param Tconstructor
         * @param db
         */
        constructor(Tconstructor: any, db: DB);
        static GetEntityName(typeConstructor: Function): string;
        GetIsDirty(): boolean;
        SetIsDirty(dirtyness: boolean): void;
        /**
         * Ensures that the data table for the repository type is present
         * @param forceRecreate If TRUE all found table data will be erased
         * @param onEnd
         */
        Init(forceRecreate?: boolean, onEnd?: (err: Error) => void): void;
        /**
         * Destroys if exists and recreates the empty table.
         * Useful to DELETE ALL ITS DATA.
         */
        RecreateTable(onEnd?: (err: Error) => void): void;
        LoadDump(dump: string, behaviour?: LOAD_DUMP_BEHAVIOUR, onEnd?: (err: Error) => void): any;
        /**
         * Checks if an Entity exists in this repository (search by PrimaryKey).
         *
         * @param pk  an Entity PrimaryKey
         * @param onEnd Called with "result" containing the check, or with non-null "err" in case of error
         */
        Exists(pk: any, onEnd: (result: boolean, err: Error) => void): void;
        /**
         * Deletes the passed entity (search by PrimaryKey) IF IT EXISTS in this repository.
         *
         * @param pk  an Entity PrimaryKey
         * @param onEnd Called with non-null "err" in case of error
         */
        Delete(pk: any, onEnd: (err: Error) => void): void;
        GetCount(onEnd: (itemsCount: number, err: Error) => void, whereClause?: string): void;
        /**
         * Returns ALL entities in this repository.
         *
         * @param onEnd Called with "data" containing the entities, or with non-null "err" in case of error
         * @param collectionsToFill Specify which collections (properties of the retrieved entities) to retrieve
         */
        GetAll(onEnd: (data: T[], err: Error) => void, collectionsToFill?: string[]): void;
        /**
         * Returns ALL entities in this repository satisfying the passed clause
         *
         * @param whereClause The SQL where clause
         * @param onEnd Called with "data" containing the entities, or with non-null "err" in case of error
         * @param collectionsToFill Specify which collections (properties of the retrieved entities) to retrieve
         * @param onlySpecifiedFields
         */
        GetWhere(whereClause: string, onEnd: (data: T[], err: Error) => void, collectionsToFill?: string[], onlySpecifiedFields?: string[]): void;
        /**
         * Returns the entity with the specified PrimaryKey in this repository.
         *
         * @param pk
         * @param onEnd Called with "data" containing the entity if found or NULL if not found,
         * or with non-null "err" in case of error
         * @param collectionsToFill
         */
        GetByPk(pk: any, onEnd: (data: T, err: Error) => void, collectionsToFill?: string[]): void;
        /**
         * Returns the entities with the specified PrimaryKeys in this repository.
         *
         * @param pks
         * @param onEnd Called with "data" containing the entities found, or with non-null "err" in case of error
         * @param collectionsToFill Specifies which collections (properties of the retrieved entities) to retrieve
         */
        GetByPks(pks: any[], onEnd: (data: T[], err: Error) => void, collectionsToFill?: string[]): void;
        /**
         * Executes an OPTIIMIZED bulk INSERT for all passed entities which MUST BE NEW.
         * WARNING: if any of them already exist then an error will be returned and NO entity will be inserted!
         *
         * @param items The NEW entities
         * @param onEnd Called with non-null "err" in case of error
         */
        InsertBulk(items: T[], onEnd?: (err: Error) => void): void;
        /**
         * Persists the passed entities.
         * Expects a Not-Null items, otherwise the callback will be called with a specific error.
         * A CHECK is made whether each item needs to be INSERTED or UPDATED.
         * WARNING: not so performing... since (2*items.length) queries are performed.
         *
         * @param items The entities to be persisted
         * @param onEnd  Called with "created" == true if INSERT, == false if UPDATE for each item, or with non-null "err" in case of error
         * @param collectionsToSave Specifies which collections (properties of the saved entities) to save
         */
        SaveAll(items: T[], onEnd?: (created: boolean[], err: Error) => void, collectionsToSave?: string[]): void;
        /**
         * Persists the passed entity.
         * Expects a Not-Null obj, otherwise the callback will be called with a specific error.
         * A CHECK is made whether it needs to be INSERTED or UPDATED.
         * WARNING: not so performing... since 2 queries are performed.
         *
         * @param obj The entity to be persisted
         * @param onEnd  Called with "created" == true if INSERT, == false if UPDATE, or with non-null "err" in case of error
         * @param collectionsToSave Specifies which collections (properties of the saved entity) to save
         */
        Save(obj: T, onEnd?: (created: boolean, err: Error) => void, collectionsToSave?: string[]): void;
        /**
         * Returns the first NON null error from the one passed and eventual errors returned by the async functions whose results have been passed
         */
        protected static GetFirstNotNullError(err: any, results: IronLibsCommon.ExecuteAndJoin.Result[]): any;
        /**
         * Tries to convert a PLAIN JavaScript object to a T-Typed instance
         */
        protected ConvertObjectToType(obj: any): T;
        /**
         * Returns the fields SQL-VALUES of the given record
         */
        protected GetSqlEntityValues(data: T): string;
        protected GetSqlCreateTable(ifNotExist: boolean): string;
    }
}
